#include <stdio.h>
#include "vnd/zed/dbg.h"
#include <stdlib.h>
#include <assert.h>

typedef struct Chunk Chunk;
struct Chunk
{
   size_t len;
   char* buf;
   size_t written;
   Chunk *link;
   int should_free;
};

Chunk* Chunk_create(char *buf, size_t len, Chunk *link, int should_free) {
    Chunk *chunk = (Chunk *)malloc(sizeof(Chunk));
    chunk->buf = buf;
    chunk->len = len;
    chunk->should_free = should_free;
    chunk->written = 0;
    chunk->link = link;
    return chunk;
}

void Chunk_destroy(Chunk *chunk) {
    if (chunk->should_free == 1) {
        debug("Freeing buffer");
        free(chunk->buf);
    }
    free(chunk);
}

typedef struct Queue {
    Chunk *front;
    Chunk *rear;
} Queue;

Queue* Queue_create() {
    Queue *queue = (Queue *)malloc(sizeof(Queue));
    queue->front = NULL;
    queue->rear = NULL;
    return queue;
}

void Queue_destroy(Queue *queue) {
    free(queue);
}

void Queue_add(Queue *queue, char *buf, size_t len, int should_free)
{
    Chunk *chunk = Chunk_create(buf, len, NULL, should_free);
    if (queue->rear == NULL) {
        queue->rear = chunk;
        queue->front = queue->rear;
    } else {
        queue->rear->link = chunk;
        queue->rear = chunk;
    }
}

int Queue_delete(Queue *queue, Chunk **chunk)
{
    if ((queue->front == queue->rear) && (queue->rear == NULL))
    {
        /* "The queue is empty */
        return -1;
    }
    *chunk = queue->front;
    queue->front = queue->front->link;
    if (queue->rear == *chunk) {
        queue->rear = NULL;
        /* Should already be NULL q->front = NULL; */
    }
    return 0;
}

