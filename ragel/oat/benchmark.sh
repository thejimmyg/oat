#!/bin/sh

if [ ! -e test.pipe ]; then
    mkfifo test.pipe
fi

if [ ! -e input ]; then
    python gen.py > input
fi

time ./benchmark_sync.sh
time ./benchmark_pipeline.sh
echo "Done."
