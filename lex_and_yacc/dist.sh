#!/bin/bash

TARGET=dist
NAME=aci
if [ -e $TARGET ]; then
    rm -fr $TARGET
fi
mkdir -p $TARGET
cd $TARGET
#queue_test rfc822_parser_test streamer_test two_parsers_test
cat > Makefile.am << EOF
bin_PROGRAMS = cmd_parser_test cmd_test
cmd_parser_test_SOURCES = cmd_parser_test.c cmd_parser.c cmd_parser.h cmd_parser.l.c cmd_parser.y.c cmd.c james.h cmd.h cmd_parser.y.h
cmd_parser_test_CFLAGS = $(AM_CFLAGS) -DDEBUG -g -O0
cmd_test_SOURCES = cmd_test.c cmd.c james.h cmd.h
cmd_test_CFLAGS = $(AM_CFLAGS) -DDEBUG -g -O0
EOF

# cmd_parser.l.c: cmd_parser.l
# 	lex --noyywrap --prefix=cmd_parser_yy $(AM_LFLAGS) $(LFLAGS) cmd_parser.l
# 	mv lex.cmd_parser_yy.c cmd_parser.l.c
# 
# cmd_parser.y.c: cmd_parser.y
# 	yacc -d -p cmd_parser_yy $(AM_YFLAGS) $(YFLAGS) cmd_parser.y
# 	mv y.tab.h cmd_parser.y.h
# 	mv y.tab.c cmd_parser.y.c

cp ../src/cmd_parser.c .
cp ../src/cmd_parser.h .
cp ../src/cmd_parser.l .
cp ../src/cmd_parser.y .
cp ../src/cmd_parser.l.c .
cp ../src/cmd_parser.y.c .
cp ../src/cmd_parser.y.h .
cp ../src/cmd_parser_test.c .

cp ../src/cmd.c .
cp ../src/cmd_test.c .
cp ../src/cmd.h .
cp ../src/james.h .

autoscan --verbose --debug
sed -e "s/FULL-PACKAGE-NAME/$NAME/" \
    -e 's/VERSION/1/' \
    -e 's|BUG-REPORT-ADDRESS|/dev/null|' \
    -e '10i\
AM_INIT_AUTOMAKE' < configure.scan > configure.ac

touch NEWS README AUTHORS ChangeLog

autoreconf -f -iv
./configure
make distcheck
cd ../

