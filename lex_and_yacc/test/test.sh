#!/bin/sh

set -e

export MAX=10000

. test/queue/test.sh
. test/cmd/test.sh
. test/cmd_parser/test.sh
. test/rfc822_parser/test.sh
. test/two_parsers/test.sh
. test/streamer/test.sh

ensure_no_leaks () {
    cmd=$1
    if ! valgrind $@ 2>&1 | grep "ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)" > /dev/null; then
        printf "Failed: errors reported by valgrind in $cmd\n"
        exit 1
    fi
}

if command -v valgrind > /dev/null; then
    echo "Checking cmd_test"
    ensure_no_leaks ./debug/cmd_test
    echo "Checking queue_test"
    ensure_no_leaks ./debug/queue_test
    echo "Checking cmd_parser_test"
    ensure_no_leaks ./debug/cmd_parser_test
    echo "Checking rfc822_parser_test"
    ensure_no_leaks ./debug/rfc822_parser_test
    echo "Checking two_parsers_test"
    ensure_no_leaks ./debug/two_parsers_test
    echo "Checking streamer_test"
    ensure_no_leaks ./debug/streamer_test
    echo "Checking streamer_select_test"
    ensure_no_leaks ./debug/streamer_select_test test/streamer/in test/streamer/out $MAX
    printf "Memory leak checks - Success\n"
else
    printf "Memory leak checks - Skipped\n"
fi
