REAMDE
======

Based on this:

* http://ds9a.nl/lex-yacc/cvs/lex-yacc-howto.html

We want to parse a grammer like this:

~~~
heat on
        Heater on!
heat off
        Heater off!
target temperature 22
        New temperature set!
~~~

Compile with:

~~~
make src/parser
~~~

See also:

* http://www.tldp.org/HOWTO/Lex-YACC-HOWTO-4.html
* http://www.ibm.com/developerworks/aix/tutorials/au-lexyacc/
