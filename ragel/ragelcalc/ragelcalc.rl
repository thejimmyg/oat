#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BUF_LEN 1024 /* Read buffer in bytes */


typedef struct {
        uint8_t cs;
        long val;
        bool success;
        bool neg;
} ragelcalc_parser;

%%{
        machine ragelcalc;
        access parser->;

        action see_neg {
            parser->neg = true;
        }

        action add_digit {
            parser->val = parser->val * 10 + (fc - '0');
        }

        main :=
            ( '-'@see_neg | '+' )? ( digit @add_digit )+
            ( '\n' )?
            $eof{ parser->success = true ; };
}%%

%% write data;

void init_ragelcalc_parser(ragelcalc_parser* parser)
{
        %% write init;
}

void parse_ragelcalc(ragelcalc_parser* parser, const char* p, uint16_t len, uint8_t is_eof )
{
        /* pe points to 1 byte beyond the end of this block of data */
        const char* pe = p + len;
        /* Indicates the end of all data, 0 if not in this block */
        const char* eof = is_eof ? pe : ((char*) 0);
        %% write exec;
}


int main(int argc, char** argv)
{
        if (argc != 1) {
                fprintf(stderr, "Usage: ragel\n");
                return 1;
        }
        ragelcalc_parser parser;
        parser.success = false;
        parser.val = 0;
        parser.neg = false;
        init_ragelcalc_parser(&parser);
        char buf[BUF_LEN+1];
        int len;
        len = read(STDIN_FILENO, buf, sizeof(buf));
        if (len == -1) {
                perror("read");
                return 1;
        } else if (len == 0) {
                fprintf(stderr, "EOF\n");
        } else {
                buf[len] = '\0';
                parse_ragelcalc(&parser, buf, len, 0);
                parse_ragelcalc(&parser, buf, 0, 1);
        }
        if ( ! parser.success ) {
                fprintf( stderr, "ragelcalc: there was an error\n" );
                return 1;
        }
        if ( parser.neg ) {
                parser.val = -1 * parser.val;
        }
        printf("Result: %ld\n", parser.val);
        return 0;
}
