aci
===

A universal approach to writing accessible software.

Jison
-----

`jison` - turns our doesn't support streaming parsing so these are only useful on the client really

* `dsv` - parse a delimiter separated values file
* `cmd` - parse a series of commands, but not in a streaming way

For each sub directory run:

~~~
npm install
make && make test
~~~


Ragel
-----

`ragel` - a streaming implementation of the jison `cmd` example

* `ragelcalc` - a tool that parses and then prints intergers - good starting point!
* `escape` - GPL2 experiments with escpaing
* `oat` - provides a backend for oAuth2 implementations

For each of these run:

~~~
make && make test
~~~

