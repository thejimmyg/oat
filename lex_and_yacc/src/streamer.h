#ifndef __streamer_h__
#define __streamer_h__


#include "queue.h"
#include <stddef.h>
#include <sys/select.h>


typedef int (*parse_cb)(
    void *parse_ctx,
    char *buf_pos,
    size_t nchars,
    size_t reserved_space
);

typedef int (*request_read_cb)(
    void* request_read_ctx,
    int fd,
    char *buf_pos,
    size_t max
);

typedef struct StreamReader StreamReader;
struct StreamReader {
    int fd;
    char* buf;
    int eof;
    int bad_parser;
    int err_count;
    int max_err_count;
    size_t min_read;
    size_t last_err_pos;
    size_t len;
    size_t reserved_space; /* In case the parser needs it for something */
    size_t offset; /* Where the parser should start from */
    size_t nbytes; /* Number of bytes filled from offset after request_read() */
    int fail_count;
    void* parse_ctx;
    parse_cb parse;
    void* request_read_ctx;
    request_read_cb request_read;
};


typedef int (*request_write_cb)(
    void* request_write_ctx,
    int fd,
    char *buf_pos,
    size_t max
);

typedef struct StreamWriter StreamWriter;
struct StreamWriter {
    int fd;
    Queue* queue;
    void* request_write_ctx;
    request_write_cb request_write;
};

StreamWriter* StreamWriter_create(
    int fd,
    void* request_write_ctx,
    request_write_cb request_write
);

#define StreamWriter_expect(N, V) if((N) != (V)) { \
    log_error( "Expected " #N " to be %zu, not %zu", (size_t)(V), (size_t)(N) ); \
}

int StreamWriter_prepare_select_writefds(
    StreamWriter* sw,
    int* maxfd,
    fd_set* fdwrite
);

int StreamWriter_handle_any_select_write(
    StreamWriter* sw,
    fd_set* fdwrite
);

int StreamWriter_write(StreamWriter* sw);

void StreamWriter_destroy(StreamWriter* sw);


StreamReader* StreamReader_create(
    int fd,
    char* buf,
    size_t len,
    size_t reserved_space,
    int max_err_count,
    void* parse_ctx,
    parse_cb parse,
    void* request_read_ctx,
    request_read_cb request_read
);

#define StreamReader_expect(N, V) if((N) != (V)) { \
    log_error( "Expected " #N " to be %zu, not %zu", (size_t)(V), (size_t)(N) );\
}

int StreamReader_read(StreamReader* sr);

void StreamReader_destroy(StreamReader* sr);

/* select() interface */
void StreamReader_prepare_select_readfds(
    StreamReader* sr,
    int* maxfd,
    fd_set* fdread
);

int StreamReader_handle_any_select_read(
    StreamReader* sr,
    fd_set* fdread
);

#endif /* __streamer_h__ */
