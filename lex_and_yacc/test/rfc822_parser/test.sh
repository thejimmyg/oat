#!/bin/sh

set -e


RESULT=$( ./debug/rfc822_parser_test 2>&1 )
CODE=$?
echo "$RESULT" > test/rfc822_parser/actual
if [ ! x"$CODE" = x"0" ]; then
    printf "Error -->${RESULT}<--\n"
    exit 1
fi

if ! diff -u test/rfc822_parser/expected test/rfc822_parser/actual; then
    printf "Failed the test"
    exit 2
else
    printf "rfc822_parser - Success\n"
fi
