#!/bin/sh

RESULT=$(./ragelcalc 2>&1 << EOF
123
EOF
)

if [ x"$?" = x"1" ]; then
    printf "ERROR, The output was:\n${RESULT}\nEOF"
fi
if ! printf "123" | ./ragelcalc 2> /dev/null > /dev/null ; then
    printf "Failed to handle 123"
fi
if ! printf "123\n" | ./ragelcalc 2> /dev/null > /dev/null ; then
    printf "Failed to handle 123\\\\n"
fi
if printf "123\n\n" | ./ragelcalc 2> /dev/null > /dev/null; then
    printf "Incorrectly accepted 123\\\\n\\\\n\n"
fi

echo "All done"
