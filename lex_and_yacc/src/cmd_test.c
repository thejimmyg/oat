#include "cmd.h"
#include "james.h"
#include <string.h>

int main() {
    Cmd *cmd = Cmd_create();
    /* Make sure strings are dynamically allocated. It will be automatically
     * freed by Cmd_destroy() */
    char* name = strdup("say");
    char* arg1 = strdup("Hello");
    char* arg2 = strdup("World!");
    cmd->name = name;
    ArgList_append(cmd->args, arg1, 5);
    ArgList_append(cmd->args, arg2, 6);
    char* text =  ArgList_to_string(cmd->args);
    test_out("Command:-->%s %s<--\n", cmd->name, text);
    free(text);
    Cmd_destroy(cmd);
    return 0;
}
