
%{
    cnt = 0; 
%}

%lex
NP                     \n\r\t
%%


[{NP}]+                  /* IGNORE */  
"%"[0-9a-fA-F][0-9a-fA-F]  { return 'PERCENT'; }
";"                        { return 'RECORDEND'; }
"|"                        { return 'FIELDEND'; }
[^|;%{NP}]+                { return 'DATA'; }
<<EOF>>                    { return 'EOF'; }
.                          { return 'INVALID'; }

/lex

%% /* language grammar */

start
    : records RECORDEND EOF %{ return {result: $1, fieldCount: cnt}; %}
    ;

records
    : record                     { $$ = [$1] }
    | records RECORDEND record   { var a = $1; a.push($3); $$ = a; } 
    ;

record
    : fields { $$ = $1 }
    ;
 
fields
    : field            { $$ = [$1] } 
    | fields FIELDEND field     { var a = $1; a.push($3); $$ = a; } 
    ;

field
    : data            %{ cnt++; $$ = $1; %}
    ;

data
    : combo            { $$ = $1}
    | data combo       { $$ = $1 + $2 }
    ; 

combo
    : DATA             { $$ = $1 }
    | PERCENT          { $$ = decodeURIComponent($1) ; }
    ;

%%
