#!/bin/sh

#set -e


>&2 printf "None\n====\n"
./debug/oat_test << END
oat test -a --onetwo "t""o" three


END


>&2 printf "\nArgs\n====\n"
printf "oat test -a --one two \"^\"\"$\" \"^\"\"\" \"\"\"$\" \"\"\"\"\"\" \"\" \n" | ./debug/oat_test


>&2 printf "\nMulti\n=====\n"
printf "oat init\noat init\n" | ./debug/oat_test


>&2 printf "\nEOF\n===\n"
./debug/oat_test << END
oat test -a --one "t""e" two << EOF
hello
EOF
END


>&2 printf "\nLength\n======\n"
./debug/oat_test << END
oat test -a --one "t""" two << 5
hello
END


>&2 printf "\nUser\n=====\n"
./debug/oat_test << END > out.txt
oat user t.u << 5
hello
END

>&2 echo "All done"
