#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "cmd_parser.h"
#include "cmd_parser.y.h"
#include "james.h"

// Not quite right
extern void * cmd_parser_yy_scan_buffer(char *, size_t);
extern int cmd_parser_yyleng;

CmdParser* CmdParser_create(char* buf, size_t len, size_t used) {
    assert(len >= used);

    CmdParser* cmd_parser = (CmdParser*) malloc(sizeof(CmdParser));
    cmd_parser->cmd = Cmd_create();
    cmd_parser->res = -1;
    cmd_parser->err_string = NULL;
    cmd_parser->cur_offset = -1;
    cmd_parser->next_char = '?';
    if (len-used < 2) {
        log_error("There is not enough free space at the end of the CmdParser buffer to add the 2 required null bytes");
        cmd_parser->res = 2;
        return cmd_parser;
    }
    // Set the last two bytes to NULL
    buf[len-1] = '\0';
    buf[len-2] = '\0';
    // Global variable defined in yacc
    cmdp = cmd_parser;
    cmd_parser_yy_scan_buffer(buf, len);
    // Back to using the other pointer
    cmd_parser->res = cmd_parser_yyparse();
    cmd_parser->cur_offset = cmd_parser_yytext - buf + cmd_parser_yyleng - 1;
    // Put the hold character back.
    buf[cmd_parser->cur_offset+1] = cmd_parser->next_char;
    cmd_parser_yylex_destroy();
    test_out(
        "cur_offset %zu, last char (%c), next char (%c)", 
        cmd_parser->cur_offset, 
        buf[cmd_parser->cur_offset],
        cmd_parser->next_char
    );
    cmdp = NULL;
    return cmd_parser;
};


// Does not free the buffer
void CmdParser_destroy(CmdParser* cmd_parser) {
    Cmd_destroy(cmd_parser->cmd);
    free(cmd_parser);
};
