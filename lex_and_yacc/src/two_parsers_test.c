#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "rfc822_parser.h"
#include "cmd_parser.h"
#include "james.h"


int main()
{

    char* buf = (char *) malloc (sizeof(char) * 14);
    char* args;
    CmdParser* cmd_parser;
    Rfc822Parser* rfc822_parser;

    printf("\nCmd parser ...\n");
    memset(buf,'$', 14);
    memcpy(buf, "oat  --oo a\n", 12);
    cmd_parser = CmdParser_create(buf, 14, 12);
    args = ArgList_to_string(cmd_parser->cmd->args);
    printf("Cmd: %s %s\n", cmd_parser->cmd->name, args);
    free(args);
    CmdParser_expect(cmd_parser->res, 0);
    CmdParser_expect(cmd_parser->cur_offset, 11);
    CmdParser_destroy(cmd_parser);
    cmd_parser = NULL;
    printf("done.\n");

    printf("\nrfc822 parser ...\n");
    memset(buf,'$', 14);
    memcpy(buf, "hello: worl\n", 12);
    rfc822_parser = Rfc822Parser_create(buf, 14, 12);
    Rfc822Parser_expect(rfc822_parser->res, 0);
    CmdParser_expect(rfc822_parser->cur_offset, 11);
    Rfc822Parser_expect(rfc822_parser->says_hello, 1);
    Rfc822Parser_destroy(rfc822_parser);
    rfc822_parser = NULL;
    printf("done.\n");

    printf("\nCmd parser again ...\n");
    memset(buf,'$', 14);
    memcpy(buf, "oat  --oo a\n", 12);
    cmd_parser = CmdParser_create(buf, 14, 12);
    args = ArgList_to_string(cmd_parser->cmd->args);
    printf("Cmd: %s %s\n", cmd_parser->cmd->name, args);
    free(args);
    CmdParser_expect(cmd_parser->res, 0);
    CmdParser_expect(cmd_parser->cur_offset, 11);
    CmdParser_destroy(cmd_parser);
    cmd_parser = NULL;
    printf("done.\n");

    free(buf);
    return 0;
}
