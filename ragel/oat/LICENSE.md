License
=======

Copyright 2015 James Gardner all rights reserved.

Some of the files in the vnd directory have separate licenses:

vnd/mkpath/mkpath.c
    Derived from mkpath.c

    (You are hereby given permission to use this code for any purpose with attribution.)
    Attribution: Jonathan Leffler
    http://stackoverflow.com/questions/675039/how-can-i-create-directory-tree-in-c-linux

vnd/mkpath/jlss.h
    Derived from jlss.h

    BSD license
    http://stackoverflow.com/questions/1006797/tried-and-true-simple-file-copying-code-in-c
