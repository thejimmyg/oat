#include "queue.h"
#include "james.h"
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>

int main()
{
     Queue *q = Queue_create();
     Chunk *chunk = NULL;
     int res;

     char* str1 = malloc(2);
     sprintf(str1, "1");
     str1[1] = '\0';

     test_out("Adding %s", str1);
     Queue_add(q, str1, 1, 1);
     test_out("Adding 2");
     Queue_add(q, "2", 1, 0);
     test_out("Adding 3");
     Queue_add(q, "3", 1, 0);
     test_out("About to delete 1 ...");
     res = Queue_delete(q, &chunk);
     assert(res == 0);
     test_out("The value deleted is %.*s", 1, chunk->buf);
     Chunk_destroy(chunk);
     test_out("Adding 4");
     Queue_add(q, "4", 1, 0);
     Queue_delete(q, &chunk);
     test_out("The value deleted is %s", chunk->buf);
     Chunk_destroy(chunk);
     res = Queue_delete(q, &chunk);
     assert(res == 0);
     test_out("The value deleted is %s", chunk->buf);
     Chunk_destroy(chunk);
     res = Queue_delete(q, &chunk);
     assert(res == 0);
     test_out("The value deleted is %s", chunk->buf);
     Chunk_destroy(chunk);
     assert(q->front == NULL);
     assert(q->rear == NULL);
     test_out("Adding 5");
     Queue_add(q, "5", 1, 0);
     res = Queue_delete(q, &chunk);
     assert(res == 0);
     test_out("The value deleted is %s", chunk->buf);
     Chunk_destroy(chunk);
     assert(q->front == NULL);
     assert(q->rear == NULL);
     res = Queue_delete(q, &chunk);
     assert(res == -1);
     Queue_destroy(q);
     return 0;
}
