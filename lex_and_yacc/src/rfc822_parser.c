#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "rfc822_parser.h"
#include "rfc822_parser.y.h"

// Not the correct return type, but it works!
extern void* rfc822_parser_yy_scan_buffer(char *, size_t);
extern int rfc822_parser_yyleng;


// Not quite right
extern void * rfc822_parser_yy_scan_buffer(char *, size_t);

Rfc822Parser* Rfc822Parser_create(char* buf, size_t len, size_t used) {
    assert(len >= used);

    Rfc822Parser* rfc822_parser = (Rfc822Parser*) malloc(sizeof(Rfc822Parser));
    rfc822_parser->says_hello = 0;
//    rfc822_parser->buf_len = -1;
    rfc822_parser->res = -1;
    rfc822_parser->err_string = NULL;
    rfc822_parser->cur_offset = -1;
    rfc822_parser->next_char = '?';
    if (len-used < 2) {
        log_error("There is not enough free space at the end of the Rfc822Parser buffer to add the 2 required null bytes");
        rfc822_parser->res = 2;
        return rfc822_parser;
    }
    // Set the last two bytes to NULL
    buf[len-1] = '\0';
    buf[len-2] = '\0';
    // Global variable defined in yacc
    rfc822p = rfc822_parser;
    rfc822_parser_yy_scan_buffer(buf, len);
    // Back to using the other pointer
    rfc822_parser->res = rfc822_parser_yyparse();
    // Put the hold character back.
    // With the multiline rules, yacc seems to add another character.
    if (rfc822_parser->res == 0) {
        rfc822_parser->cur_offset = rfc822_parser_yytext - buf + rfc822_parser_yyleng - 2;
        buf[rfc822_parser->cur_offset+2] = rfc822_parser->next_char;
    } else {
        rfc822_parser->cur_offset = rfc822_parser_yytext - buf + rfc822_parser_yyleng - 1;
        buf[rfc822_parser->cur_offset+1] = rfc822_parser->next_char;
    }
    //rfc822_parser_yy_flush_buffer();
    rfc822_parser_yylex_destroy();
    test_out(
        "cur_offset %zu, last char (%c), next char (%c)", 
        rfc822_parser->cur_offset, 
        buf[rfc822_parser->cur_offset],
        rfc822_parser->next_char
    );
    rfc822p = NULL;
    return rfc822_parser;
};


// Does not free the buffer
void Rfc822Parser_destroy(Rfc822Parser* rfc822_parser) {
    free(rfc822_parser);
};


