#include "james.h"

extern char* rfc822_parser_yytext;
extern int rfc822_parser_yylex_destroy(void);
extern int rfc822_parser_yyparse(void);
extern int rfc822_parser_yylex(void);
extern void rfc822_parser_yyerror(const char*);

typedef struct Rfc822Parser Rfc822Parser;
struct Rfc822Parser {
    int res;
    const char* err_string;
    char next_char;
    size_t cur_offset;
    int says_hello;
    //int lex_error;
//    size_t buf_len;
};

Rfc822Parser* Rfc822Parser_create( char* buf, size_t len, size_t used);
 
void Rfc822Parser_destroy(
    Rfc822Parser* rfc822_parser
);

#define Rfc822Parser_expect(N, V) if((N) != (V)) { \
    log_error( "Expected " #N " to be %zu, not %zu", (size_t)(V), (size_t)(N) ); \
    assert((N) == (V)); \
}

/* Nasty global variable for yacc */

Rfc822Parser* rfc822p;
