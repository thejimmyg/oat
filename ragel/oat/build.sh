#!/bin/bash

if [ -e oat.c ]; then
    rm oat.c
fi
make oat.c
if [ -e autodemo ]; then
    rm -r autodemo
fi
mkdir -p autodemo
cd autodemo
cat > Makefile.am << EOF
bin_PROGRAMS=oat
oat_SOURCES=oat.c
oat_CFLAGS=-Wall -Wextra -std=c99 -O3 -g

EOF

cp ../oat.c .
cp ../oat.rl .
autoscan
sed -e 's/FULL-PACKAGE-NAME/oat/' \
    -e 's/VERSION/1/' \
    -e 's|BUG-REPORT-ADDRESS|/dev/null|' \
    -e '10i\
AM_INIT_AUTOMAKE' < configure.scan > configure.ac

# AC_PROG_CC
touch NEWS README AUTHORS ChangeLog

autoreconf -iv
./configure
make distcheck
cd ../

