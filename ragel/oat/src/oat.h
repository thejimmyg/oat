#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct {
    const char **array;
    size_t used;
    size_t size;
    size_t *item_sizes;
} ArgList;

typedef struct {
    size_t name_len;
    char* name;
    ArgList *args;
} Cmd;

typedef struct {
    int type;
} Ctx;


typedef void (*strategy_cb)(Ctx *ctx, Cmd *cmd, int strategy, size_t length);
typedef void (*data_cb)(Ctx *ctx, Cmd *cmd, const char* byte);
typedef void (*end_cb)(Ctx *ctx, Cmd *cmd);
typedef void (*cmd_cb)(Cmd *cmd, const char *string, int length);

typedef struct {
    /* Custom parser variables */
    Cmd *cmd;
    Ctx *ctx;
    char* real_buf;
    // We would need one per overlapping value, but we don't have any luckily, so we share this one.
    size_t BUF_LEN;
    const char* mark_pos;
    int marked_len;
    int consumed_so_far;
    int first;
    // For iterating through the length of data
    long datalen;
    long datalen_charcount;
    /* Ragel variables */
    uint8_t cs;
    // For handling the fcall for parsing quotedvalue
    // int top;
    // int stack[];

    //cmd_cb sub_cmd_name;
    // cmd_cb short_option;
    // cmd_cb long_option;
    // cmd_cb unquoted_argument;
    // cmd_cb quoted_argument;
    cmd_cb length;
    strategy_cb strategy;
    data_cb data;
    end_cb end;

} OatParser;


OatParser* OatParser_create(Ctx *ctx, strategy_cb strategy_cb, data_cb data_cb, end_cb end_cb, size_t BUF_LEN);
int OatParser_on_data_available(int fd, OatParser *parser);
void OatParser_destroy(OatParser *parser);
