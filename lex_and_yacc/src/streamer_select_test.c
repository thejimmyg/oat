#include "james.h"
#include "streamer.h"
#include "queue.h"
#include <unistd.h> // for read() and write()
#include <stdlib.h>
#include <sys/select.h>
#include <fcntl.h>


#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "cmd_parser.h"

// #include <limits.h>
// #include <errno.h>
// #include <assert.h>


typedef struct ParseCtx ParseCtx;
struct ParseCtx {
    StreamWriter* sw;
    StreamReader* sr;
    int state;
    char* number;
    int used;
    long max;
    size_t length;
    char* real_buf; /* You wouldn't do this normally, I'm just using it to calculate an offset for the tests */
};


typedef struct RequestReadCtx RequestReadCtx;
struct RequestReadCtx {
    char* real_buf; /* You wouldn't do this normally, I'm just using it to calculate an offset for the tests */
};


int parse(void *parse_ctx, char *buf_pos, size_t nbytes, size_t reserved_space) {
    ParseCtx *pctx = (ParseCtx*)parse_ctx;
    pctx->number = (char*)malloc(10);
    test_out("Parse");
    test_out("Parse buf_pos is at %zu", buf_pos - pctx->real_buf);
    test_out("Parse nbytes is %zu", nbytes);
    test_out("Parse reserved_space is %zu", reserved_space);
    test_out("Parse state is %d", pctx->state);
    if (nbytes > 30) {
        test_out("ALL >%.*s<...>%.*s<", 20, buf_pos, 20, &buf_pos[nbytes-20]);
    } else {
        test_out("ALL >%.*s<", (int)nbytes, buf_pos);
    }
    if (pctx->state == 1) {
        test_out("STATE 1");
        // We are parsing the cmd part
        CmdParser* cmd_parser = CmdParser_create(buf_pos, nbytes+reserved_space, nbytes);
        int res = cmd_parser->res;
        if (res == 0) {
            test_out("cmd_parse result: %d", res);
            //test_out("Length parsed: %zu", cmd_parser->next_char);
            test_out("Args used: %zu", cmd_parser->cmd->args->used);
            assert(cmd_parser->cmd->args->used == 3);
            pctx->length = strtol(
                cmd_parser->cmd->args->array[cmd_parser->cmd->args->used-1],
                NULL,
                10
            );
            test_out("Expecting a length of %zu", pctx->length);
            //test_out("Parsed the command, telling the streamer to start at %zu next time", cmd_parser->next_char);
            //test_out("The buffer at that length would be -->%.*s<--", (int)cmd_parser->next_char, buf_pos);
            int ret_val = cmd_parser->cur_offset + 1;
            CmdParser_destroy(cmd_parser);
            cmd_parser = NULL;
            //test_out("done parsing the buffer of -->%.*s<--", (int)nbytes, buf_pos);
            pctx->state = 2;
            pctx->used = 0;
            return ret_val;
        } else {
            CmdParser_destroy(cmd_parser);
            test_out("Couldn't parse, returning 0");
            return 0;
        }
    } else if (pctx->state == 2) {
        test_out("STATE 2");
        test_out("Got %zu bytes, need %zu", nbytes, pctx->length);
        if (nbytes >= pctx->length) {

            memcpy(&pctx->number[pctx->used], buf_pos, pctx->length);
            pctx->used += pctx->length;
            pctx->number[pctx->used] = '\0';
            long body = strtol(
                (const char *)pctx->number,
                NULL,
                10
            );
            if (body == pctx->max) {
                printf("Found %ld\n", body);
                pctx->sr->eof = 1;
            }
            pctx->number[pctx->used] = '\n';
            pctx->number[pctx->used+1] = '\0';
            test_out("Adding the data is -->%.*s<-- length %d to the queue.", (int)pctx->used+1, pctx->number, pctx->used);
            Queue_add(pctx->sw->queue, pctx->number, pctx->used+1, 0);

            int len = pctx->length;
            pctx->length = 0;
            pctx->state = 1;
            pctx->used = 0;
            return len;
        } else {
            test_out("Not enough data in the buffer. Adding just the first %zu bytes -->%.*s<-- to the queue.", nbytes, (int)nbytes, buf_pos);
            //Queue_add(pctx->sw->queue, buf_pos, nbytes, 0);
            pctx->length = pctx->length - nbytes;
            pctx->used += nbytes;
            memcpy(pctx->number, buf_pos, nbytes);
            return nbytes;
        }
    } else {
        test_out("ERROR State: %d", pctx->state);
        exit(1);
    }
}


int request_read(void *request_read_ctx, int fd, char *buf_pos, size_t max) {
    RequestReadCtx *rctx = (RequestReadCtx*)request_read_ctx;
    test_out("Read");
    test_out("Buffer is at %zu", buf_pos - rctx->real_buf);
    test_out("Max is %zu", max);
    return read(fd, buf_pos, max);
}


int request_write(void *request_write_ctx, int fd, char *buf_pos, size_t max) {
    test_out("Write");
    return write(fd, buf_pos, max);
}


int main(int argc, char** argv) {
    assert(argc == 4);
    size_t read_buf_len = (128*1024)-2;
    size_t reserved_space = 2;
    int max_err_count = 5;
    char* buf = (char *) malloc (sizeof(char)*(read_buf_len+reserved_space));

    int in_fd;
    if (strcmp(argv[1], "-") == 0) {
        in_fd = 0;
    } else {
        in_fd = open(argv[1], O_RDWR);
    }
    int out_fd = open(argv[2], O_WRONLY | O_CREAT, S_IRWXU);

    StreamWriter* sw = StreamWriter_create(out_fd, NULL, &request_write);

    ParseCtx* parse_ctx = (ParseCtx *) malloc (sizeof(ParseCtx));
    parse_ctx->real_buf = buf;
    parse_ctx->sw = sw;
    parse_ctx->state = 1;
    parse_ctx->max = strtol(
        argv[3],
        NULL,
        10
    );
    test_out("Expecting a max length of %lu", parse_ctx->max);

    RequestReadCtx* request_read_ctx = (RequestReadCtx *) malloc (sizeof(RequestReadCtx));
    request_read_ctx->real_buf = buf;

    StreamReader *sr = StreamReader_create(
        in_fd,
        buf,
        read_buf_len,
        reserved_space,
        max_err_count,
        parse_ctx,
        &parse,
        request_read_ctx,
        &request_read
    );
    parse_ctx->sr = sr;

    struct timeval timeout;
    fd_set fdread, fdwrite;
    int timeoutms;
    int ret, len, write_len;
    int maxfd;
    int no_read_errors = 1;
    int data_to_write = 0;

    while ( no_read_errors || data_to_write ) {
        test_out("no_read_errors: %d, data_to_write: %d", no_read_errors, data_to_write);
        timeoutms = 100; /* With this set to 0, things behave strangely with pipes */
        //timeoutms = 0;
        maxfd = -1;
        timeout.tv_sec = 0;
        timeout.tv_usec = 0;
        /* Wait on stdin for input */
        FD_ZERO(&fdread);
        FD_ZERO(&fdwrite);


        data_to_write = StreamWriter_prepare_select_writefds(sw, &maxfd, &fdwrite);
        StreamReader_prepare_select_readfds(sr, &maxfd, &fdread);

        if (timeoutms >= 0) {
            timeout.tv_sec = timeoutms / 1000;
            if (timeout.tv_sec > 1) {
                /* Let's not give the plugins too much control */
                timeout.tv_sec = 1;
            } else {
                timeout.tv_usec = (timeoutms % 1000) * 1000;
            }
        }
        ret = select(maxfd + 1, &fdread, &fdwrite, NULL, &timeout);
        if (ret == -1){
            log_error("Error calling select: %d", ret);
            //break;
        } else if ( ret == 0 ) {
            //debug("%d milliseconds elapsed.", timeoutms);
        } else {
            StreamWriter_handle_any_select_write(sw, &fdwrite);
            no_read_errors = StreamReader_handle_any_select_read(sr, &fdread);
            if (no_read_errors < 0) {
                printf("ERROR: %d\n", no_read_errors);
                exit(3);
            }
            if (sr->eof == 1 && sw->queue->front == NULL) {
                printf("Finished writing\n");
                break;//exit(0);
            }
        }
        test_out("Select looping");
    }
    StreamReader_destroy(sr);
    StreamWriter_destroy(sw);
    free(parse_ctx);
    free(request_read_ctx);
    free(buf);
    write(in_fd, "\4", 1);
    close(in_fd);
    close(out_fd);
    printf("Reading and Writing complete\nExiting\n");
    return 1;
}
