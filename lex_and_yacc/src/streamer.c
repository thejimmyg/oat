#include "streamer.h"
#include "james.h"
#include <stdlib.h>
#include <assert.h>
#include <sys/select.h>


void StreamReader_prepare_select_readfds(StreamReader* sr, int* maxfd, fd_set* fdread) {
    if (sr->eof == 0) {
        FD_SET(sr->fd, fdread);
        if (sr->fd > *maxfd) {
            *maxfd = sr->fd;
        }
    }
}


int StreamReader_handle_any_select_read(StreamReader* sr, fd_set* fdread) {
    if (FD_ISSET(sr->fd, fdread)) {
        return StreamReader_read(sr);
    } else {
        test_out("Nothing to read!");
        return 1;
    }
}

int StreamReader_read(StreamReader* sr) {
    if (sr->eof == 1) {
        test_out("EOF");
        return 0;
    }
    if (sr->bad_parser) {
        test_out("Bad parser");
        return -2;
    }
    test_out("%zu %zu %zu %zu", sr->offset, sr->nbytes, sr->reserved_space, sr->len);
    if (sr->offset + sr->nbytes + sr->reserved_space == sr->len) {
        test_out("Already full");
        return -3;
    }
    if (sr->offset + sr->nbytes + sr->reserved_space > sr->len) {
        test_out("Buffer overflow");
        return -5;
    }
    test_out("Something to read! Can read up to %zu bytes.", sr->len-sr->reserved_space-sr->offset-sr->nbytes);
    int read_len = sr->request_read(sr->request_read_ctx, sr->fd, &sr->buf[sr->offset+sr->nbytes], sr->len-sr->reserved_space-sr->offset-sr->nbytes);
    if (read_len < 1) {
        if (read_len == 0) {
            sr->eof = 1;
            test_out("EOF");
            return 0;
        } else {
            test_out("Error reading %d", read_len);
            return -1;
        }
    } else {
        test_out("Read %d byte(s) into buffer", read_len);
        int last_parse = 0;
        // Now do the parsing
        int max_bytes = sr->nbytes + read_len;
        while (1) {
            if (sr->eof == 1) {
                return 0;
            }
            int res = sr->parse(sr->parse_ctx, &sr->buf[sr->offset], max_bytes, sr->reserved_space);
            test_out("Parse result %d", res);
            if (res < 0 || res > max_bytes) {
                test_out("Bad parser %d", res);
                sr->bad_parser = 1;
                return -2;
            } else if (res == 0) {
                test_out("res is 0");
                sr->err_count++;
                sr->nbytes += read_len;
                if (sr->nbytes + sr->reserved_space == sr->len) {
                    test_out("Buffer is now full. Returning error");
                    // Reset for next run
                    sr->err_count = 0;
                    sr->nbytes = 0;
                    sr->offset = 0;
                    return -3;
                } else {
                    size_t available = (sr->len - sr->reserved_space) - sr->nbytes;
                    size_t available_at_end = available - sr->offset;
                    if (last_parse) {
                        // We've come to the end of the process of sending more parse requests without a read, we just need to return what we've got
                        test_out("Finished parsing the data from this read (last_parse is %d, nbytes is %zu, offset is %zu)", last_parse, sr->nbytes, sr->offset);
                        //sr->nbytes = 0;
                        //sr->err_count = 0;
                        // BUT we need to make sure the next read parse takes place at the right place (the end of the
                        // offset)
                        // The best way to do that is to shuffle the current bytes back to the start and 
                        // read from there.
                        test_out("%zu bytes space in total, with %zu are at the front of the buffer - Shuffling down %zu bytes...", available, sr->offset, sr->nbytes);
                        memmove(sr->buf, &sr->buf[sr->offset], sr->nbytes);
                        sr->offset = 0;
                        return last_parse;
                    } 
                    test_out("More data needed for next parse, %zu in total now, %zu available at the end", sr->nbytes, available_at_end);
                    if (available_at_end >= sr->min_read) {
                        test_out("Let's just carry on filling the buffer from where we are");
                        return last_parse;
                   // } else if (available_at_end + sr->offset > sr->min_read) {
                   //     test_out("%zu bytes space in total, with %zu are at the front of the buffer - Shuffling ...", available, sr->offset);
                   //     memmove(sr->buf, &sr->buf[sr->offset], sr->nbytes);
                   //     sr->offset = 0;
                   //     return -1;
                   //     //return last_parse;
                    } else {
                        test_out("Not enough free space to be worth shuffling the buffer.");
                        // Reset for next run
                        sr->err_count = 0;
                        sr->nbytes = 0;
                        sr->offset = 0;
                        return -4;
                    }
                }
            } else {
                test_out("Parsed %d bytes", res);
                sr->err_count = 0;
                if (res == max_bytes) {
                    test_out("Exhausted the data available, resetting the buffer back to %d for the next read", 0);
                    sr->offset = 0;
                    sr->nbytes = 0;
                    return last_parse+res;
                } else {
                    test_out("Last parse: %d, res: %d. New last_parse: %d", last_parse, res, last_parse+res);
                    last_parse += res;
                    test_out("Moving the offset to the end of the %d parsed bytes", res);
                    sr->offset = sr->offset + res;
                    sr->nbytes += read_len - res;
                    // Max bytes for next time must be the number of bytes left
                    max_bytes = sr->nbytes;
                    // Don't count the read length if we loop again - XXX not in the tests yet!
                    read_len=0;
                    // No return value, we want to loop
                }
            }
        }
    }
}

StreamWriter* StreamWriter_create(int fd, void* request_write_ctx, request_write_cb request_write) {
    StreamWriter *sw = (StreamWriter *) malloc (sizeof(StreamWriter));
    sw->queue = Queue_create();
    sw->fd = fd;
    sw->request_write_ctx = request_write_ctx;
    sw->request_write = request_write;
    test_out("StreamWriter creation complete.");
    return sw;
};

void StreamWriter_destroy(StreamWriter* sw) {
    test_out("Destroyed StreamWriter");
    Queue_destroy(sw->queue);
    sw->queue=NULL;
    free(sw);
}

int StreamWriter_prepare_select_writefds(StreamWriter* sw, int* maxfd, fd_set* fdwrite) {
    if (sw->queue->front != NULL) {
         test_out("Setting the write fd");
         FD_SET(sw->fd, fdwrite);
         if (sw->fd > *maxfd) {
             *maxfd = sw->fd;
         }
         return 1;
    } else {
        test_out("Nothing to prepare for write!");
        return 0;
    }
}

int StreamWriter_handle_any_select_write(StreamWriter* sw, fd_set* fdwrite) {
    if (FD_ISSET(sw->fd, fdwrite)) {
        return StreamWriter_write(sw);
    } else {
        test_out("No write fd ready to write");
        return 1;
    }
}

int StreamWriter_write(StreamWriter* sw) {
    Chunk *chunk = sw->queue->front;
    int write_len = chunk->len - chunk->written;
    int len = sw->request_write(sw->request_write_ctx, sw->fd, &chunk->buf[chunk->written], write_len);
    if (len <= 0) {
        return len;
    } else {
        log_info("Written %d bytes", len);
        chunk->written = chunk->written + len;
        if (chunk->written == chunk->len) {
            Queue_delete(sw->queue, &chunk);
            Chunk_destroy(chunk);
            //chunk->callback(0, chunk->context);
        }
        return len;
    }
}

StreamReader* StreamReader_create(int fd, char* buf, size_t len, size_t reserved_space, int max_err_count, void* parse_ctx, parse_cb parse, void* request_read_ctx, request_read_cb request_read) {
    assert(reserved_space < len);
    StreamReader* sr = (StreamReader *) malloc (sizeof(StreamReader));
    sr->fd = fd;
    sr->eof = 0;
    sr->bad_parser = 0;
    sr->buf = buf;
    sr->min_read = 11;
    sr->len = len;
    sr->max_err_count = max_err_count;
    sr->err_count = 0;
    sr->last_err_pos = -1;
    sr->reserved_space = reserved_space;
    sr->offset = 0;
    sr->nbytes = 0;
    sr->fail_count = 0;
    sr->parse_ctx = parse_ctx;
    sr->parse = parse;
    sr->request_read_ctx = request_read_ctx;
    sr->request_read = request_read;
    test_out("StreamReader creation complete.");
    return sr;
}

void StreamReader_destroy(StreamReader* sr) {
    test_out("Destroyed StreamReader");
    free(sr);
}
