#include <stddef.h>

typedef struct Chunk Chunk;
struct Chunk
{
   size_t len;
   char* buf;
   size_t written;
   Chunk *link;
   int should_free;
};

Chunk* Chunk_create(char *buf, size_t len, Chunk *link, int should_free);

void Chunk_destroy(Chunk *chunk);

typedef struct Queue {
    Chunk *front;
    Chunk *rear;
} Queue;

Queue* Queue_create();

void Queue_destroy(Queue *queue);

void Queue_add(Queue *queue, char *buf, size_t len, int should_free);

int Queue_delete(Queue *queue, Chunk **chunk);
