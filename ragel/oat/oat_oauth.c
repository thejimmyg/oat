#include "oat.h"

#include <getopt.h>
#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <signal.h>

#include "vnd/mkpath/mkpath.c"

#define INIT_CTX_TYPE 1000
#define BUF_LEN 7 /* Read buffer in bytes */

typedef struct Chunk Chunk;

struct Chunk
{
   size_t len;
   char* buf;
   size_t written;
   Chunk *link;
   int should_free;
};

Chunk* Chunk_create(char *buf, size_t len, Chunk *link, int should_free) {
    Chunk *chunk = (Chunk *)malloc(sizeof(Chunk));
    chunk->buf = buf;
    chunk->len = len;
    chunk->should_free = should_free;
    chunk->written = 0;
    chunk->link = link;
    return chunk;
}

void Chunk_destroy(Chunk *chunk) {
    if (chunk->should_free == 1) {
        debug("Freeing buffer");
        free(chunk->buf);
    }
    free(chunk);
}

typedef struct Queue {
    Chunk *front;
    Chunk *rear;
} Queue;

Queue* Queue_create() {
    Queue *queue = (Queue *)malloc(sizeof(Queue));
    queue->front = NULL;
    queue->rear = NULL;
    return queue;
}

void Queue_destroy(Queue *queue) {
    free(queue);
}

void Queue_add(Queue *queue, char *buf, size_t len, int should_free)
{
    Chunk *chunk = Chunk_create(buf, len, NULL, should_free);
    if (queue->rear == NULL) {
        queue->rear = chunk;
        queue->front = queue->rear;
    } else {
        queue->rear->link = chunk;
        queue->rear = chunk;
    }
}

int Queue_delete(Queue *queue, Chunk **chunk)
{
    if ((queue->front == queue->rear) && (queue->rear == NULL))
    {
        /* "The queue is empty */
        return -1;
    }
    *chunk = queue->front;
    queue->front = queue->front->link;
    if (queue->rear == *chunk) {
        queue->rear = NULL;
        /* Should already be NULL q->front = NULL; */
    }
    return 0;
}

void test_queue()
{
     Queue *q = Queue_create();
     Chunk *chunk = NULL;
     int res;

     char* str1 = malloc(2);
     sprintf(str1, "1");
     str1[1] = '\0';

     char* str2 = malloc(2);
     sprintf(str2, "2");
     str2[1] = '\0';

     char* str3 = malloc(2);
     sprintf(str3, "3");
     str3[1] = '\0';

     char* str4 = malloc(2);
     sprintf(str4, "4");
     str4[1] = '\0';

     char* str5 = malloc(2);
     sprintf(str5, "5");
     str5[1] = '\0';

     printf("Adding %s\n", str1);
     Queue_add(q, str1, 1, 1);
     printf("Adding 2\n");
     Queue_add(q, "2", 1, 0);
     printf("Adding 3\n");
     Queue_add(q, "3", 1, 0);
     printf("About to delete 1 ...\n");
     res = Queue_delete(q, &chunk);
     assert(res == 0);
     printf("The value deleted is %.*s\n", 1, chunk->buf);
     Chunk_destroy(chunk);
     printf("Adding 4\n");
     Queue_add(q, "4", 1, 0);
     Queue_delete(q, &chunk);
     printf("The value deleted is %s\n", chunk->buf);
     Chunk_destroy(chunk);
     res = Queue_delete(q, &chunk);
     assert(res == 0);
     printf("The value deleted is %s\n", chunk->buf);
     Chunk_destroy(chunk);
     res = Queue_delete(q, &chunk);
     assert(res == 0);
     printf("The value deleted is %s\n", chunk->buf);
     Chunk_destroy(chunk);
     assert(q->front == NULL);
     assert(q->rear == NULL);
     printf("Adding 5\n");
     Queue_add(q, "5", 1, 0);
     res = Queue_delete(q, &chunk);
     assert(res == 0);
     printf("The value deleted is %s\n", chunk->buf);
     Chunk_destroy(chunk);
     assert(q->front == NULL);
     assert(q->rear == NULL);
     res = Queue_delete(q, &chunk);
     assert(res == -1);
     Queue_destroy(q);
}



typedef struct {
    Ctx super;
    const char* config;
    int fd;
    Queue* queue_out;
} InitCtx;


void help(char c) {
    if (c != 'h') {
        log_err("error");
    }
    debug("This is the help text");
}


void on_end(Ctx *ctx, Cmd *cmd) {
    assert(ctx->type == INIT_CTX_TYPE);
    InitCtx *initctx = (InitCtx*)ctx;
    if (initctx->fd != -1) {
        debug("Closing %d", initctx->fd);
        close(initctx->fd);
        initctx->fd = -1;
    }
    if (strncmp("user", cmd->name, cmd->name_len + 1) == 0) {
        char *buf = "oat resp --ok\n";
        Queue_add(initctx->queue_out, buf, strlen(buf), 0);
    }
}

void on_data(Ctx *ctx, Cmd *cmd, const char* byte) {
    assert(ctx->type == INIT_CTX_TYPE);
    InitCtx *initctx = (InitCtx*)ctx;
    if (initctx->fd != -1) {
        debug("Writing to %d: %.*s", initctx->fd, 1, byte);
        int len;
        len = write(initctx->fd, byte, 1);
        if (len == -1) {
            perror("Write error");
            exit(6);
        } else if (len == 0) {
            /* Shouldn't ever be able to get here */
            log_err("Write error: No data");
            exit(7);
        } else {
            debug("Written: %d %.*s", len, len, byte);
        }
    }
}

void on_strategy(Ctx *ctx, Cmd *cmd, int strategy, size_t length) {
    int x;
    log_info("Args found: %zu", cmd->args->used);
    for (x=0; x<cmd->args->used; x++) {
        debug("Arg %d, (%zu bytes): -->%s<--", x, cmd->args->item_sizes[x], cmd->args->array[x]);
    }
    assert(ctx->type == INIT_CTX_TYPE);
    InitCtx *initctx = (InitCtx*)ctx;
    // We'll include the null-terminator in the comparison to stop 'init' matching 'initialize' for example:
    if (strncmp("init", cmd->name, cmd->name_len + 1) == 0) {
        debug("Handling init for %s", initctx->config);
        if (mkpath(initctx->config, 0777) != 0) {
            log_err("%d: failed to create (%d: %s): %s", (int)getpid(), errno, strerror(errno), initctx->config);
            exit(5);
        }
        /* longest name + 2 because of the '/' and the terminating 0 */
        char *fullpath = malloc(strlen(initctx->config) + strlen("accessTokenByUserIdAndClient") + 2);
        const char *dir_strs[] = {
            "userById",
            "userByUsername",
            "client",
            "accessToken",
            "accessTokenByUserIdAndClient",
            "refreshToken"
        };
        int x;
        for ( x = 0; x < 6; x++ ) {
            sprintf(fullpath, "%s/%s", initctx->config, dir_strs[x]);
            if (mkpath(fullpath, 0777) != 0) {
                log_err("%d: failed to create (%d: %s): %s", (int)getpid(), errno, strerror(errno), fullpath);
                exit(5);
            }
        }
        free(fullpath);
        debug("Done handling init for %s", initctx->config);
    } else if (strncmp("user", cmd->name, cmd->name_len + 1) == 0) {
        debug("Handling user %s with %zu args", initctx->config, cmd->args->used);
        assert(cmd->args->used == 1);
        int len = cmd->args->item_sizes[0]+2+strlen(initctx->config);
        char *userpath = malloc(sizeof(char)*len);
        sprintf(userpath, "%s/%s", initctx->config, cmd->args->array[0]);
        initctx->fd = open(userpath, O_WRONLY | O_CREAT, S_IRWXU);
        debug("Opened file %s as fd %d", userpath, initctx->fd);
        free(userpath);
    }
}


int main (int argc, char **argv) {
    // test_queue();
    int c;
    int digit_optind = 0;
    int aopt = 0, bopt = 0;
    char *copt = 0, *dopt = 0;
    static struct option long_options[] = {
        {"help", no_argument, NULL, 'h'},
        {NULL, 0, NULL, 0}
    };
    int option_index = 0;
    while ((c = getopt_long(argc, argv, "h",
       long_options, &option_index)) != -1) {
        int this_option_optind = optind ? optind : 1;
        switch (c) {
        // case 0:
        //     debug ("option %s", long_options[option_index].name);
        //     if (optarg)
        //             debug (" with arg %s", optarg);
        //     debug ("\n");
        //     break;
        case 'h':
            help(c);
            return 0;
        case '?':
            break;
        default:
            help(c);
        }
    }
    if (optind == argc) {
        log_err("No storage directory specified");
        return 5;
    } else if (optind < argc - 1) {
        log_err("Unexpected arguments");
        return 4;
        // debug ("non-option ARGV-elements: ");
        // while (optind < argc)
        //     debug ("%s ", argv[optind++]);
        // debug ("\n");
    } else {
        //debug("optind: %d", optind);
        /* Global variable */
        InitCtx ctx;
        ctx.super.type = INIT_CTX_TYPE;
        ctx.fd = -1;
        ctx.config = argv[optind];
        ctx.queue_out = Queue_create();
        OatParser *parser = OatParser_create(&ctx.super, &on_strategy, &on_data, &on_end, BUF_LEN);
        debug("Max length: %d",  BUF_LEN);

        struct timeval timeout;
        fd_set fdread, fdwrite;
        int timeoutms;
        int ret, res, len, write_len;
        int maxfd;
        res = 1;
        while(res > 0) {
            timeoutms = 1000000; /* With this set to 0, things behave strangely with pipes */
            maxfd = -1;
            timeout.tv_sec = 0;
            timeout.tv_usec = 0;
            /* Wait on stdin for input */
            FD_ZERO(&fdread);
            FD_ZERO(&fdwrite);
            FD_SET(0, &fdread);
            if (0 > maxfd) {
                maxfd = 0;
            }
            if (ctx.queue_out->front != NULL) {
                 log_info("Got some data to write to stdout");
                 FD_SET(1, &fdwrite);
                 if (1 > maxfd) {
                     maxfd = 1;
                 }
            }
            if (timeoutms >= 0) {
                timeout.tv_sec = timeoutms / 1000;
                if (timeout.tv_sec > 1) {
                    /* Let's not give the plugins too much control */
                    timeout.tv_sec = 1;
                } else {
                    timeout.tv_usec = (timeoutms % 1000) * 1000;
                }
            }
            ret = select(maxfd + 1, &fdread, &fdwrite, NULL, &timeout);
            if (ret == -1){
                log_err("select");
                break;
            } else if ( ret == 0 ) {
                //debug("%d milliseconds elapsed.", timeoutms);
            } else {
                if (FD_ISSET(0, &fdread)) {
                    res = OatParser_on_data_available(parser);
                }
                if (FD_ISSET(1, &fdwrite)) {
                    Chunk *chunk = ctx.queue_out->front;
                    write_len = chunk->len - chunk->written;
                    len = write(1, &chunk->buf[chunk->written], write_len);
                    if (len <= 0) {
                        /* "Error during write" */
                        exit(5);
                    }
                    log_info("Written %d bytes", len);
                    chunk->written = chunk->written + len;
                    if (chunk->written == chunk->len) {
                            Queue_delete(ctx.queue_out, &chunk);
                            Chunk_destroy(chunk);
                            //chunk->callback(0, chunk->context);
                    }
                }
            }
        }
        OatParser_destroy(parser);
    }
}
