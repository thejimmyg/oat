#include "cmd.h"

extern char* cmd_parser_yytext;
extern int cmd_parser_yylex_destroy(void);
extern int cmd_parser_yyparse(void);
extern int cmd_parser_yylex(void);
extern void cmd_parser_yyerror(const char*);
// Not the correct return type, but it works!


typedef struct CmdParser CmdParser;
struct CmdParser {
    int res;
    const char* err_string;
    char next_char;
    size_t cur_offset;
    Cmd* cmd;
};

CmdParser* CmdParser_create();

void CmdParser_destroy(
    CmdParser* cmd_parser
);

#define CmdParser_expect(N, V) if((N) != (V)) { \
    log_error( "Expected " #N " to be %zu, not %zu", (size_t)(V), (size_t)(N) ); \
    assert((N) == (V)); \
}

/* Horrible global vairiable used by yacc */
CmdParser* cmdp;
