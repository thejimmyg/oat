%{
#include <stdio.h>
#include <string.h>
#include "cmd_parser.y.h"
#include "cmd_parser.h"

%}

%x here_doc

%option noyywrap

%%

"<<"                        { cmdp->next_char=yy_hold_char; BEGIN(here_doc); return TOKARROW; }
[ ]+                        { cmdp->next_char=yy_hold_char; return TOKSPACE; }
"!"                         { cmdp->next_char=yy_hold_char; return TOKEXCL; }
[\n]+                       { cmdp->next_char=yy_hold_char; return TOKNEWLINE; }
"-"[a-zA-z0-9]+             { cmdp->next_char=yy_hold_char; cmd_parser_yylval.string=strdup(yytext); return SHORTOPT; }
"--"[a-zA-z0-9]+            { cmdp->next_char=yy_hold_char; cmd_parser_yylval.string=strdup(yytext); return LONGOPT; }
[a-zA-z_]+                  { cmdp->next_char=yy_hold_char; cmd_parser_yylval.string=strdup(yytext); return CMDNAME; }
[a-zA-Z0-9]+                { cmdp->next_char=yy_hold_char; cmd_parser_yylval.string=strdup(yytext); return ARG;}
<here_doc>[A-Z]+            { cmdp->next_char=yy_hold_char; cmd_parser_yylval.string=strdup(yytext); return EOFLABEL; }
<here_doc>[0-9]+            { cmdp->next_char=yy_hold_char; cmd_parser_yylval.string=strdup(yytext); return LENGTH; }
<here_doc>[ ]+              { cmdp->next_char=yy_hold_char; return TOKSPACE; }
<here_doc>[\n]+             { cmdp->next_char=yy_hold_char; BEGIN(INITIAL); return TOKNEWLINE; }
.                           { cmdp->next_char=yy_hold_char; cmd_parser_yyerror("unexpected character"); return ERROR; }
%%
