all: jison ragel

jison: jison/dsv jison/cmd

jison/dsv:
	cd jison/dsv && make

jison/cmd:
	cd jison/cmd && make

ragel: 
	cd ragel && make
