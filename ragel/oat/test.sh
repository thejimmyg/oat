#!/bin/sh

set -e


if printf "oat init --toolongopt" | ./debug/oat_test test_dir/new 2> /dev/null; then
    printf "Failed to exit due to overflowing buffer\n"
    exit 1
fi

RESULT=$( ./debug/queue_test 2>&1 )
if [ ! x"$?" = x"0" ]; then
    printf "Error -->${RESULT}<--\n"
    exit 1
fi

EXPECTED=$(cat expected_queue_test)
if [ ! x"$RESULT" = x"$EXPECTED" ]; then
    printf "Failed the test"
    printf "$RESULT" > actual_queue_test
    diff -u expected_queue_test actual_queue_test
    printf "Failed the test"
else
    printf "Success\n"
fi


RESULT=$(./actual.sh 2>&1 )
if [ x"$?" = x"1" ]; then
    printf "Error -->${RESULT}<--\n"
    exit 1
fi


EXPECTED=$(cat expected_oat_test)
if [ ! x"$RESULT" = x"$EXPECTED" ]; then
    printf "Failed the test"
    printf "$RESULT" > actual_oat_test
    diff -u expected_oat_test actual_oat_test
    printf "Failed the test"
else
    printf "Success\n"
fi


RESULT=$( xxd out.txt )
if [ ! x"$RESULT" = x"0000000: 6f61 7420 7265 7370 202d 2d6f 6b0a       oat resp --ok." ]; then
    printf "Failed the out.txt test -->$RESULT<--"
else
    printf "Success\n"
fi


# Check there is no output to stdout
RESULT=$(./actual.sh 2> /dev/null )
if [ ! x"$RESULT" = x"" ]; then
    printf "Error some output was sent to stdout -->${RESULT}<--\n"
    exit 1
fi

