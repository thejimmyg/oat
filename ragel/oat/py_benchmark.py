import subprocess
import os
import locale
import time

def connect(args):
    # Could test shell=True, executable=...
    connection = subprocess.Popen(
        args,
        shell=False,
        stdout=subprocess.PIPE,
        stdin=subprocess.PIPE,
        #stderr=subprocess.PIPE,
    )
    return connection

if __name__ == '__main__':
    try:
        locale.setlocale(locale.LC_ALL, os.environ.get('LANG', 'en_US'))
    except:
        pass

    connection = connect([
        "release/oat_echo_server",
    ])
    start = time.time()
    count = 100000
    for x in range(count):
        connection.stdin.write('oat echo << EOF\nhello\nEOF\n')
    c = 0
    while 1:
        c += 1
        line = connection.stdout.readline()
        if c == 1:
            print line
        if c == count:
            break
    end = time.time()
    print "Req/sec: {} for {} requests in {} secs".format(
        count/(end - start),
        locale.format("%d", count, grouping=True),
        end-start,
        grouping=True,
    )
