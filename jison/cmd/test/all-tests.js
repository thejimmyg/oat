var cmd = require('../cmd'),
    expect = require('chai').expect;

describe('Tests', function(){
  it('should run', function(){
    var source = require('fs').readFileSync('example/cmds', "utf8");
    var result = cmd.parse(source);
    expect(result).to.deep.equal([
        {subcmd: 'gc', opts: []},
        {subcmd: 'gc', opts: []},
        {subcmd: 'init', opts: [ 'abc', 'a\n{\\"a' ]},
        {subcmd: 'gc', opts: [], "stdin": [ "one", " EOF ", "three" ], }
    ]);
  })
})
