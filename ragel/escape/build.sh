#!/bin/bash

if [ -e escape.c ]; then
    rm escape.c
fi
make escape.c
if [ -e autodemo ]; then
    rm -r autodemo
fi
mkdir -p autodemo
cd autodemo
cat > Makefile.am << EOF
bin_PROGRAMS=escape
escape_SOURCES=escape.c
escape_CFLAGS=-Wall -Wextra -std=c99 -O3 -g

EOF

cp ../escape.c .
cp ../escape.rl .
autoscan
sed -e 's/FULL-PACKAGE-NAME/escape/' \
    -e 's/VERSION/1/' \
    -e 's|BUG-REPORT-ADDRESS|/dev/null|' \
    -e '10i\
AM_INIT_AUTOMAKE' < configure.scan > configure.ac

# AC_PROG_CC
touch NEWS README AUTHORS ChangeLog

autoreconf -iv
./configure
make distcheck
cd ../

