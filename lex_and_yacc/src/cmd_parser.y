%{
#include <stdio.h>
#include <string.h>
#include "james.h"
#include "cmd_parser.h"

// int yydebug = 0;
#define YYERROR_VERBOSE 1

void cmd_parser_yyerror(const char *str)
{
    log_info("CmdParser error: %s\n", str);
    if (cmdp->err_string == NULL) {
        cmdp->err_string = str;
    }
}

int cmd_parser_yywrap()
{
    return 1;
}


%}

%token TOKSPACE TOKARROW TOKNEWLINE ERROR TOKEXCL

%union
{
	int number;
	char *string;
}

%token <string> SHORTOPT
%token <string> ARG
%token <string> LONGOPT
%token <string> LENGTH
%token <string> EOFLABEL
%token <string> CMDNAME

%type <string> arg

%start command

%%

command
    : error {
        YYABORT;
    }
    | TOKNEWLINE CMDNAME TOKSPACE args TOKNEWLINE {
        cmdp->cmd->name=$2;
        YYACCEPT;
    }
    | CMDNAME TOKSPACE args TOKNEWLINE {
        cmdp->cmd->name=$1;
        YYACCEPT;
    }
    ;

args
    : arg {
         ArgList_append(cmdp->cmd->args, $1, strlen($1));
    }
    | args TOKSPACE arg {
         ArgList_append(cmdp->cmd->args, $3, strlen($3));
    }
    ;

arg
    : ARG
    | CMDNAME   {
        // This might be accidentaly matched instead of ARG, that's fine
    }
    | SHORTOPT
    | LONGOPT
    ;
