%{
%}

%lex

%x oat
%x option
%x data

// NP                     \n\r\t

%%


"oat"                     { this.begin('oat'); return 'OAT'; }
<oat>[ ]+                 { return 'SP'; }

// Must be defined before options
<oat>("gc")               { return 'OAT_GC' }
<oat>("init")             { return 'OAT_INIT' }

<oat>("\"")               { this.begin('option'); }
<oat>[^"\s<\\\[\]\{\}]]+  { return 'OPTION'; }
<option>(\\\"|[^"])+      { return 'OPTION'; }
<option>("\"")            { this.popState(); }
<oat>\n                   { this.popState(); return 'LF'; }

<oat>("<<EOF")            { this.begin('data'); return 'DATA_START'}
<data>("EOF")             { this.popState(); return 'DATA_END' }
<data>\n                  { return 'DATA_LF' }
<data>[^\n]+              { return 'DATA' }

<<EOF>>                   { return 'EOF'; }
// Useful for debugging:
// .                      { console.log('ERROR: unexpected "' + yytext + '" character') }


/lex


%start start


%%


start
    : cmds LF EOF { return $1 }
    ;

cmds
    : cmd { $$ = [$1] }
    | cmds LF cmd { var a = $1; a.push($3); $$ = a; }
    ;

cmd
    : simplecmd
    | stdincmd
    ;

stdincmd
    : simplecmd DATA_START DATA_LF lines DATA_LF DATA_END { $1['stdin'] = $4; $$ = $1 }
    ;

lines
    : line { $$ = [$1] }
    | lines DATA_LF line { var a = $1; a.push($3); $$ = a; }
    ;

line
    : DATA { $$ = $1 }
    ;

simplecmd
    : OAT SP subcmd SP options  %{ $$ = {subcmd: $3, opts: $5} %}
    | OAT SP subcmd             %{ $$ = {subcmd: $3, opts: []} %}
    | OAT SP subcmd SP          %{ $$ = {subcmd: $3, opts: []} %}
    ;

subcmd
    : OAT_GC
    | OAT_INIT
    ;

options
    : option { $$ = [$1] }
    | options SP option { var a = $1; a.push($3); $$ = a; }
    ;

option
    : OPTION { $$ = $1 }
    ;

%%
