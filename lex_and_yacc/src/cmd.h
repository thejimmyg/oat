#ifndef __cmd_h__
#define __cmd_h__

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>


typedef struct {
    const char **array;
    size_t used;
    size_t size;
    size_t *item_sizes;
} ArgList;


typedef struct {
    size_t name_len;
    char* name;
    ArgList *args;
} Cmd;


Cmd* Cmd_create();
void Cmd_destroy(Cmd *cmd);
void ArgList_append(ArgList *a, const char* element, size_t len);
char *ArgList_to_string(ArgList *a);

#endif /* __cmd_h__ */
