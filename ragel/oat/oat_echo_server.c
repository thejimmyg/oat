#include "src/oat.h"
#include "src/vnd/zed/dbg.h"
#include "src/queue.h"

#include <getopt.h>
#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <signal.h>


#define INIT_CTX_TYPE 1000
#define BUF_LEN 1024 /* Read buffer in bytes */

int counter = 0;

#define MAX_MSGS 100*1000 /* Set to 0 to disable */

typedef struct {
    Ctx super;
    char buf[BUF_LEN+1];
    int len;
    Queue* queue_out;
} InitCtx;


void help(char c) {
    if (c != 'h') {
        log_err("error");
    }
    debug("This is the help text");
}


void on_end(Ctx *ctx, Cmd *cmd) {
    assert(ctx->type == INIT_CTX_TYPE);
    InitCtx *initctx = (InitCtx*)ctx;
    assert(strncmp("echo", cmd->name, cmd->name_len + 1) == 0);
    char *buf = "oat resp --ok\n";
    Queue_add(initctx->queue_out, buf, strlen(buf), 0);
    // if (initctx->len != 0) { 
    //     char *buf = strndup(initctx->buf, initctx->len);
    //     debug("About to write %d bytes", initctx->len);
    //     buf[initctx->len] = '\0';
    //     Queue_add(initctx->queue_out, buf, initctx->len, 1);
    //     initctx->len = 0;
    // } 
    // char *buf = "\nEOF\n";
    // Queue_add(initctx->queue_out, buf, 5, 0);
}


void on_data(Ctx *ctx, Cmd *cmd, const char* byte) {
    assert(ctx->type == INIT_CTX_TYPE);
    InitCtx *initctx = (InitCtx*)ctx;
    assert(strncmp("echo", cmd->name, cmd->name_len + 1) == 0);
    // if (initctx->len == BUF_LEN) { 
    //     char *buf = strndup(initctx->buf, BUF_LEN);
    //     buf[BUF_LEN] = '\0';
    //     Queue_add(initctx->queue_out, buf, BUF_LEN, 1);
    //     initctx->len = 0;
    // } else {
    //     initctx->buf[initctx->len] = byte[0];
    //     initctx->len++;
    // }
}


void on_strategy(Ctx *ctx, Cmd *cmd, int strategy, size_t length) {
    int x;
    log_info("Args found: %zu", cmd->args->used);
    for (x=0; x<cmd->args->used; x++) {
        debug("Arg %d, (%zu bytes): -->%s<--", x, cmd->args->item_sizes[x], cmd->args->array[x]);
    }
    assert(ctx->type == INIT_CTX_TYPE);
    InitCtx *initctx = (InitCtx*)ctx;
    //fprintf(stderr, "cmd: -->%s<-- %zu\n", cmd->name, cmd->name_len);
    if (cmd->name_len != 4) {
        fprintf(stderr, "Not 4: %zu %.*s\n", cmd->name_len, (int)cmd->name_len, cmd->name); 
    }
    assert(cmd->name[4] == '\0');
    assert(cmd->name_len == 4);
    assert(cmd->args->used == 0);
    //fprintf(stderr, "on_strategy %s\n", cmd->name); 
    assert(strncmp("echo", cmd->name, cmd->name_len) == 0);
}


int main (int argc, char **argv) {
    // test_queue();
    int c;
    int digit_optind = 0;
    int aopt = 0, bopt = 0;
    char *copt = 0, *dopt = 0;
    static struct option long_options[] = {
        {"help", no_argument, NULL, 'h'},
        {NULL, 0, NULL, 0}
    };
    int option_index = 0;
    while ((c = getopt_long(argc, argv, "h",
       long_options, &option_index)) != -1) {
        int this_option_optind = optind ? optind : 1;
        switch (c) {
        // case 0:
        //     debug ("option %s", long_options[option_index].name);
        //     if (optarg)
        //             debug (" with arg %s", optarg);
        //     debug ("\n");
        //     break;
        case 'h':
            help(c);
            return 0;
        case '?':
            break;
        default:
            help(c);
        }
    }
    if (argc - optind < 1) {
        log_err("No input specified");
        return 5;
    } else if (argc-optind < 2) {
        log_err("No output specified");
        return 6;
    } else if (argc-optind > 2) {
        log_err("Unexpected arguments");
        return 4;
        // debug ("non-option ARGV-elements: ");
        // while (optind < argc)
        //     debug ("%s ", argv[optind++]);
        // debug ("\n");
    } else {
        //debug("optind: %d", optind);
        /* Global variable */
        InitCtx initctx;
        initctx.super.type = INIT_CTX_TYPE;
        initctx.len = 0;
        initctx.queue_out = Queue_create();
        OatParser *parser = OatParser_create(&initctx.super, &on_strategy, &on_data, &on_end, BUF_LEN);
        debug("Max length: %d",  BUF_LEN);

        int in_fd;
        int out_fd;
        if (strncmp(argv[argc-1], "-", 1) == 0) {
            out_fd = STDOUT_FILENO;
            fprintf(stderr, "Writing to stdout: fd %d\n", out_fd);
        } else {
            fprintf(stderr, "Writing to %s: fd %d\n", argv[argc-1], out_fd);
            out_fd = open(argv[argc-1], O_RDWR);
        }
        if (strncmp(argv[argc-2], "-", 1) == 0) {
            in_fd = STDIN_FILENO;
            fprintf(stderr, "Reading from stdin: fd %d\n", in_fd);
        } else {
            in_fd = open(argv[argc-2], O_RDWR);
            fprintf(stderr, "Reading from %s: fd %d\n", argv[argc-2], in_fd);
        }


        struct timeval timeout;
        fd_set fdread, fdwrite;
        int timeoutms;
        int ret, res, len, write_len;
        int maxfd;
        res = 1;
        while(res > 0 || initctx.queue_out->front != NULL) {
            timeoutms = 1000000; /* With this set to 0, things behave strangely with pipes */
            maxfd = -1;
            timeout.tv_sec = 0;
            timeout.tv_usec = 0;
            /* Wait on stdin for input */
            FD_ZERO(&fdread);
            FD_ZERO(&fdwrite);
            FD_SET(in_fd, &fdread);
            if (in_fd > maxfd) {
                maxfd = in_fd;
            }
            if (initctx.queue_out->front != NULL) {
                 log_info("Got some data to write to stdout");
                 FD_SET(out_fd, &fdwrite);
                 if (out_fd > maxfd) {
                     maxfd = out_fd;
                 }
            }
            if (timeoutms >= 0) {
                timeout.tv_sec = timeoutms / 1000;
                if (timeout.tv_sec > 1) {
                    /* Let's not give the plugins too much control */
                    timeout.tv_sec = 1;
                } else {
                    timeout.tv_usec = (timeoutms % 1000) * 1000;
                }
            }
            ret = select(maxfd + 1, &fdread, &fdwrite, NULL, &timeout);
            if (ret == -1){
                log_err("select");
                break;
            } else if ( ret == 0 ) {
                // debug("%d milliseconds elapsed, nothing to read from fd %d or write to fd %d", timeoutms, in_fd, out_fd);
            } else {
                if (FD_ISSET(in_fd, &fdread)) {
                    res = OatParser_on_data_available(in_fd, parser);
                }
                if (FD_ISSET(out_fd, &fdwrite)) {
                    Chunk *chunk = initctx.queue_out->front;
                    write_len = chunk->len - chunk->written;
                    len = write(out_fd, &chunk->buf[chunk->written], write_len);
                    if (len <= 0) {
                        /* "Error during write" */
                        exit(5);
                    }
                    log_info("Written %d bytes", len);
                    chunk->written = chunk->written + len;
                    if (chunk->written == chunk->len) {
                        Queue_delete(initctx.queue_out, &chunk);
                        Chunk_destroy(chunk);
                        counter += 1;
                        if (MAX_MSGS && counter == MAX_MSGS) {
                            fprintf(stderr, "Received and sent %d msgs\n", counter);
                            printf("\4");
                            exit(4);
                        }
                        //chunk->callback(0, chunk->context);
                    }
                }
            }
        }
        log_warn("Exiting loop - RES: %d", res);
        Queue_destroy(initctx.queue_out);
        initctx.queue_out = NULL;
        OatParser_destroy(parser);
        parser = NULL;
        debug("Freed queue_out and oat_parser");
        close(in_fd);
        close(out_fd);
    }
}
