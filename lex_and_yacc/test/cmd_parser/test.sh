#!/bin/sh

set -e


RESULT=$( ./debug/cmd_parser_test 2>&1 )
CODE=$?
echo "$RESULT" > test/cmd_parser/actual
if [ ! x"$CODE" = x"0" ]; then
    printf "Error -->${RESULT}<--\n"
    exit 1
fi

if ! diff -u test/cmd_parser/expected test/cmd_parser/actual; then
    printf "Failed the test"
    exit 2
else
    printf "cmd_parser - Success\n"
fi
