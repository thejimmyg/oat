var dsv = require('../dsv'),
    expect = require('chai').expect;

describe('Tests', function(){
  it('should run', function(){
    var source = require('fs').readFileSync('example/data.dsv', "utf8");
    var result = dsv.parse(source);
    console.log(result);
    expect(result).to.deep.equal({result: [ [ '1', '2', '3' ], [ '4', '5', '%', '6' ] ], fieldCount: 7});
  })
})
