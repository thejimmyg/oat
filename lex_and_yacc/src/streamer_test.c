#include "james.h"
#include "streamer.h"
#include "queue.h"
#include <unistd.h> // for read() and write()
#include <stdlib.h>
#include <sys/select.h>
#include <fcntl.h>


#include <stdio.h>
#include <string.h>
#include <assert.h>


typedef struct ParseCtx ParseCtx;
struct ParseCtx {
    StreamWriter* sw;
    char* real_buf; /* You wouldn't do this normally, I'm just using it to calculate an offset for the tests */
};


typedef struct RequestReadCtx RequestReadCtx;
struct RequestReadCtx {
    char* real_buf; /* You wouldn't do this normally, I'm just using it to calculate an offset for the tests */
};

typedef struct Fixture Fixture;
struct Fixture {
    StreamReader* sr;
    StreamWriter* sw;
    ParseCtx* parse_ctx;
    RequestReadCtx* request_read_ctx;
    char* buf;
};

// Mock variables that get changed by the test cases
char*  mock_read_buf;
size_t mock_read_len;
size_t mock_write_len;
int    mock_bytes_parsed;

// Expectations that can be set by the test cases
size_t expected_parse_nbytes = -1;
size_t expected_parse_offset = -1;
size_t expected_parse_reserved_space = 2;
size_t expected_read_max = -1;
size_t expected_read_offset = -1;

// Mock variables for the second parse run
int    parse_run = 1;
int    second_mock_bytes_parsed;
size_t second_expected_parse_nbytes = -1;
size_t second_expected_parse_offset = -1;
size_t second_expected_parse_reserved_space = 2;

// Dummy values that are checked to be passed around correctly but which don't change
int dummy_in_fd = -1;
int dummy_out_fd = -2;


int parse(void *parse_ctx, char *buf_pos, size_t nbytes, size_t reserved_space) {
    ParseCtx *pctx = (ParseCtx*)parse_ctx;
    test_out("Parse buf_pos is at %zu", buf_pos - pctx->real_buf);
    test_out("Parse nbytes is %zu", nbytes);
    test_out("Parse reserved_space is %zu", reserved_space);
    assert(parse_run < 3);
    if (parse_run == 1) {
        assert(expected_parse_reserved_space == reserved_space);
        assert(expected_parse_nbytes == nbytes);
        assert(expected_parse_offset == buf_pos - pctx->real_buf);
        parse_run = 2;
        return mock_bytes_parsed;
    } else {
        assert(second_expected_parse_reserved_space == reserved_space);
        assert(second_expected_parse_nbytes == nbytes);
        assert(second_expected_parse_offset == buf_pos - pctx->real_buf);
        test_out("Returning a second_mock_bytes_parsed of %d", second_mock_bytes_parsed);
        return second_mock_bytes_parsed;
    }
}


int mocked_read(int fd, char *buf_pos, size_t max) {
    assert(parse_run == 1);
    if (mock_read_len > 0) {
        test_out("Reading data into buf_pos");
        memcpy(buf_pos, mock_read_buf, mock_read_len);
    }
    return mock_read_len;
}


int request_read(void *request_read_ctx, int fd, char *buf_pos, size_t max) {
    RequestReadCtx *rctx = (RequestReadCtx*)request_read_ctx;
    assert(fd == dummy_in_fd);
    test_out("Read buf_pos is at %zu (expected %zu)", buf_pos - rctx->real_buf, expected_read_offset);
    test_out("Read max is %zu (expected %zu)", max, expected_read_max);
    assert(expected_read_max == max);
    assert(expected_read_offset == buf_pos - rctx->real_buf);
    return mocked_read(fd, buf_pos, max);
}


int request_write(void *request_write_ctx, int fd, char *buf_pos, size_t max) {
    assert(fd == dummy_out_fd);
    test_out("Write");
    return mock_write_len;
}


int handle_any_read(StreamReader* sr, int can_read) {
    // The real implementation also checks (sr->eof == 0)
    if (can_read) {
        return StreamReader_read(sr);
    } else {
        test_out("Nothing to read!");
        return 0;
    }
}


int handle_any_write(StreamWriter* sw, int can_write) {
    if (can_write) {
        return StreamWriter_write(sw);
    } else {
        test_out("Nothing to write!");
        return 0;
    }
}


Fixture* setup() {
    size_t read_buf_len = 1024;
    size_t reserved_space = 2;
    int max_err_count = 5;
    char* buf = (char *) malloc (sizeof(char)*(read_buf_len+reserved_space));

    StreamWriter* sw = StreamWriter_create(dummy_out_fd, NULL, &request_write);

    ParseCtx* parse_ctx = (ParseCtx *) malloc (sizeof(ParseCtx));
    parse_ctx->real_buf = buf;
    parse_ctx->sw = sw;

    RequestReadCtx* request_read_ctx = (RequestReadCtx *) malloc (sizeof(RequestReadCtx));
    request_read_ctx->real_buf = buf;

    StreamReader *sr = StreamReader_create(
        dummy_in_fd,
        buf,
        read_buf_len,
        reserved_space,
        max_err_count,
        parse_ctx,
        &parse,
        request_read_ctx,
        &request_read
    );

    Fixture* fixture = (Fixture*)malloc(sizeof(Fixture));
    fixture->sr = sr;
    fixture->sw = sw;
    fixture->parse_ctx = parse_ctx;
    fixture->request_read_ctx = request_read_ctx;
    fixture->buf = buf;
    buf[0] = '\0';
    return fixture;
}


int test_consecutive_successful_read_and_parse(Fixture* f) {
    // First run: Nothing to output, but let's read some data
    parse_run = 1;
    char* first = "oat -a --b c\n\n";
    mock_read_len = 14;
    mock_read_buf = strndup(first, mock_read_len);
    mock_read_buf[14] = '\0';
    assert(strlen(mock_read_buf) == 14);
    expected_read_offset = 0;
    expected_read_max = 1022;
    expected_parse_nbytes = 14;
    expected_parse_offset = 0;
    mock_bytes_parsed = 14; // We got to the end
    assert(handle_any_read(f->sr, 1) == 14);
    assert(handle_any_write(f->sw, 0) == 0);
    assert(strncmp(f->buf, mock_read_buf, mock_read_len) == 0);
    free(mock_read_buf);
    // Let's read the smallest possible amount of data, 1 byte
    parse_run = 1;
    char* second = "-";
    mock_read_len = 1;
    mock_read_buf = strndup(second, mock_read_len);
    mock_read_buf[1] = '\0';
    assert(mock_read_len == strlen(mock_read_buf));
    expected_read_offset = 0;
    expected_read_max = 1022;
    expected_parse_nbytes = 1;
    expected_parse_offset = 0;
    mock_bytes_parsed = 1;
    assert(handle_any_read(f->sr, 1) == 1);
    assert(handle_any_write(f->sw, 0) == 0);
    free(mock_read_buf);
    return 0;
}


int test_parser_partial_read(Fixture* f) {
    // First run: Nothing to output, but let's read some data
    parse_run = 1;
    char* first = "oat -a --b c\n\n";
    mock_read_len = 14;
    mock_read_buf = strndup(first, mock_read_len);
    mock_read_buf[14] = '\0';
    assert(strlen(mock_read_buf) == 14);
    expected_read_offset = 0;
    expected_read_max = 1022;
    expected_parse_nbytes = 14;
    expected_parse_offset = 0;
    mock_bytes_parsed = 13; // We got what we needed, leaving one byte left
    second_expected_parse_nbytes = 1;
    second_expected_parse_offset = 13;
    second_mock_bytes_parsed = 1;
    assert(handle_any_read(f->sr, 1) == 14);
    assert(handle_any_write(f->sw, 0) == 0);
    assert(strncmp(f->buf, mock_read_buf, mock_read_len) == 0);
    free(mock_read_buf);
    return 0;
};


int test_full_buffer(Fixture* f) {
    int i;
    parse_run = 1;
    mock_read_buf = (char*)malloc(1024);
    mock_read_len = 1022;
    for (i=0; i<1022; i++) {
        mock_read_buf[i] = (i%10) + 65;
    }
    mock_read_buf[1022] = '\0';
    assert(strlen(mock_read_buf) == 1022);
    expected_read_offset = 0;
    expected_read_max = 1022;
    expected_parse_nbytes = 1022;
    expected_parse_offset = 0;
    mock_bytes_parsed = 1022;
    assert(handle_any_read(f->sr, 1) == 1022);
    assert(handle_any_write(f->sw, 0) == 0);
    assert(f->sr->eof == 0);
    // Now fill it again, but this time return parse 0
    parse_run = 1;
    assert(strlen(mock_read_buf) == 1022);
    expected_read_offset = 0;
    expected_read_max = 1022;
    expected_parse_nbytes = 1022;
    expected_parse_offset = 0;
    mock_bytes_parsed = 0; // This means the buffer must be full.
    assert(handle_any_read(f->sr, 1) == -3);
    assert(handle_any_write(f->sw, 0) == 0);
    assert(f->sr->eof == 0);
    // Fill it with one byte spare, check that we do the shuffle
    parse_run = 1;
    mock_read_len = 1011;
    mock_read_buf[1011] = '\0';
    assert(strlen(mock_read_buf) == 1011);
    expected_read_offset = 0;
    expected_read_max = 1022;
    expected_parse_nbytes = 1011;
    expected_parse_offset = 0;
    mock_bytes_parsed = 0;
    assert(handle_any_read(f->sr, 1) == 0); // Just within the bounds of needing more data
    assert(handle_any_write(f->sw, 0) == 0);
    free(mock_read_buf);
    // If we try to add another byte, it should need a shuffle:
    parse_run = 1;
    char* second = "-";
    mock_read_len = 1;
    mock_read_buf = strndup(second, mock_read_len);
    mock_read_buf[1] = '\0';
    expected_read_offset = 1011;
    expected_read_max = 11;
    expected_parse_nbytes = 1012;
    expected_parse_offset = 0;
    mock_bytes_parsed = 0;
    assert(handle_any_read(f->sr, 1) == -4); // Not enough free space to be worth shuffling the buffer.
    assert(handle_any_write(f->sw, 0) == 0);
    free(mock_read_buf);
    // If we try to add another byte, it should need a shuffle:
    test_out("Let's try a situation that is worth shuffling");
    //printf("f->buf: %s, f->sr->offset %zu, f->sr->nbytes %zu\n", f->buf, f->sr->offset, f->sr->nbytes);
    parse_run = 1;
    char* first = "oat -a --b c\noat -d -e f\n";
    mock_read_len = 25;
    mock_read_buf = strndup(first, mock_read_len);
    mock_read_buf[25] = '\0';
    assert(strlen(mock_read_buf) == 25);
    expected_read_offset = 0;
    expected_read_max = 1022;
    expected_parse_nbytes = 25;
    expected_parse_offset = 0;
    mock_bytes_parsed = 13; 
    // This triggers a parse loop
    second_expected_parse_nbytes = 12; // Leave one byte
    second_expected_parse_offset = 13;
    second_mock_bytes_parsed = 0; // Make sure we don't return though
    assert(handle_any_read(f->sr, 1) == 13);
    assert(handle_any_write(f->sw, 0) == 0);
    //printf("f->buf: %s, mock_read_buf: %s, f->sr->offset %zu, f->sr->nbytes %zu", f->buf, mock_read_buf, f->sr->offset, f->sr->nbytes);
    assert(strncmp(f->buf,"oat -d -e f\n", 12) == 0);
    free(mock_read_buf);
    assert(f->sr->offset == 0);
    assert(f->sr->nbytes == 12);
    return 0;
    //test_out("Adding loads more data");
    //parse_run = 1;
    //mock_read_buf = (char*)malloc(1024);
    //mock_read_len = 1021;
    //for (i=0; i<mock_read_len; i++) {
    //    mock_read_buf[i] = (i%10) + 65;
    //}
    //mock_read_buf[mock_read_len] = '\0';
    //assert(strlen(mock_read_buf) == mock_read_len);
    //expected_read_offset = 1;
    //expected_read_max = 1021;
    //expected_parse_nbytes = 1022;
    //expected_parse_offset = 1;
    //mock_bytes_parsed = 0; // We haven't found the end yet.
    //assert(handle_any_read(f->sr, 1) == 0); // There is still space at the end
    //assert(handle_any_write(f->sw, 0) == 0);
    //free(mock_read_buf);
    //test_out("Let's try adding another byte");
    //// If we try to add another byte though, it should need a shuffle:
    //parse_run = 1;
    //char* third = "-";
    //mock_read_len = 1;
    //mock_read_buf = strndup(third, mock_read_len);
    //mock_read_buf[1] = '\0';
    //expected_read_offset = 1011;
    //expected_read_max = 11;
    //expected_parse_nbytes = 999;
    //expected_parse_offset = 13;
    //mock_bytes_parsed = 0;
    ////printf("Buffer before: ^%.*s$\n", 999, &f->sr->buf[f->sr->offset]);
    //assert(handle_any_read(f->sr, 1) == -1); // Let's shuffle!
    //assert(handle_any_write(f->sw, 0) == 0);
    //// At this point, let's check the bytes are the shuffled ones:
    //assert(f->sr->offset == 0);
    //assert(f->sr->nbytes == 999);
    //char* expected = (char*)malloc(1000);
    //for (i=0; i<999; i++) {
    //    expected[i] = (i%10) + 65;
    //}
    //expected[999] = '\0';
    //expected[998] = '-'; // From the last byte
    //f->sr->buf[999] = '\0';
    ////printf("Buffer       : ^%.*s$\n", 999, f->sr->buf);
    ////printf("Expected     : ^%.*s$\n", 999, expected);
    //assert(strncmp(f->sr->buf, expected, 999) == 0);
    //free(mock_read_buf);
    //free(expected);
    //return 0;
}


int test_eof_read(Fixture *f){
    parse_run = 1;
    char* first = "oat -a --b c\n\n-";
    mock_read_len = 15;
    mock_read_buf = strndup(first, mock_read_len);
    mock_read_buf[15] = '\0';
    assert(strlen(mock_read_buf) == 15);
    expected_read_offset = 0;
    expected_read_max = 1022;
    expected_parse_nbytes = 15;
    expected_parse_offset = 0;
    mock_bytes_parsed = 0; // We didn't get to the end
    assert(handle_any_read(f->sr, 1) == 0);
    assert(handle_any_write(f->sw, 0) == 0);
    assert(strncmp(f->buf, mock_read_buf, mock_read_len) == 0);
    free(mock_read_buf);
    // Let's handle an EOF
    assert(f->sr->eof == 0);
    //mock_read_buf = (char*)malloc(1);
    parse_run = 1;
    mock_read_len = 0;
    mock_read_buf = "";
    expected_read_offset = 15;
    expected_read_max = 1007;
    expected_parse_nbytes = -1; // No parse should be called
    expected_parse_offset = -1; // No parse should be called
    mock_bytes_parsed = -10; // Shouldn't be needed
    assert(handle_any_read(f->sr, 1) == 0);
    assert(handle_any_write(f->sw, 0) == 0);
    assert(f->sr->eof == 1);
    assert(strncmp(f->buf, "oat -a --b c\n\n-", 15) == 0);
    // Let's check that a further read doesn't result in parse being called
    parse_run = 1;
    mock_read_len = 0;
    mock_read_buf = "";
    expected_read_offset = 15;
    expected_read_max = 1007;
    expected_parse_nbytes = -1; // No parse should be called
    expected_parse_offset = -1; // No parse should be called
    mock_bytes_parsed = -10; // Shouldn't be needed
    assert(handle_any_read(f->sr, 1) == 0);
    assert(handle_any_write(f->sw, 0) == 0);
    assert(f->sr->eof == 1);
    assert(strncmp(f->buf, "oat -a --b c\n\n-", 15) == 0);
    return 0;
}


int test_parser_overflow(Fixture* f) {
    parse_run = 1;
    assert(f->sr->bad_parser == 0);
    char* first = "oat -a --b c\n\n";
    mock_read_len = 14;
    mock_read_buf = strndup(first, mock_read_len);
    mock_read_buf[14] = '\0';
    assert(strlen(mock_read_buf) == 14);
    expected_read_offset = 0;
    expected_read_max = 1022;
    expected_parse_nbytes = mock_read_len;
    expected_parse_offset = 0;
    mock_bytes_parsed = 15;
    assert(handle_any_read(f->sr, 1) == -2);
    assert(f->sr->bad_parser == 1);
    assert(handle_any_write(f->sw, 0) == 0);
    assert(strncmp(f->buf, mock_read_buf, mock_read_len) == 0);
    // Check future reads return the same
    parse_run = 1;
    mock_bytes_parsed = 14;
    assert(handle_any_read(f->sr, 1) == -2);
    free(mock_read_buf);
    return 0;
}


int test_parser_bad_error(Fixture* f) {
    parse_run = 1;
    assert(f->sr->bad_parser == 0);
    char* first = "oat -a --b c\n\n";
    mock_read_len = 14;
    mock_read_buf = strndup(first, mock_read_len);
    mock_read_buf[14] = '\0';
    assert(strlen(mock_read_buf) == 14);
    expected_read_offset = 0;
    expected_read_max = 1022;
    expected_parse_nbytes = mock_read_len;
    expected_parse_offset = 0;
    mock_bytes_parsed = -1;
    assert(handle_any_read(f->sr, 1) == -2);
    assert(f->sr->bad_parser == 1);
    assert(handle_any_write(f->sw, 0) == 0);
    assert(strncmp(f->buf, mock_read_buf, mock_read_len) == 0);
    // Check future reads return the same
    parse_run = 1;
    mock_bytes_parsed = 14;
    assert(handle_any_read(f->sr, 1) == -2);
    free(mock_read_buf);
    return 0;
}


int test_parser_increments(Fixture* f) {
    // First run: Nothing to output, but let's read some data
    char* fragments[4] = {
        "oa",
        "t ",
        "-a",
        " -",
    };
    int i;
    for (i=0; i<4; i++) {
        test_out("Fragment %d", i);
        parse_run = 1;
        mock_read_len = 2;
        mock_read_buf = strndup(fragments[i], mock_read_len);
        mock_read_buf[2] = '\0';
        assert(strlen(mock_read_buf) == 2);
        expected_read_offset = i*2;
        expected_read_max = 1022-(i*2);
        expected_parse_nbytes = (i+1)*2;
        expected_parse_offset = 0;
        if (i < 3) {
            // Tell the streamer that we don't have enough
            mock_bytes_parsed = 0;
            assert(handle_any_read(f->sr, 1) == 0);
            assert(handle_any_write(f->sw, 0) == 0);
        } else {
            // This time we've got it
            mock_bytes_parsed = 8;
            assert(handle_any_read(f->sr, 1) == 8);
            assert(handle_any_write(f->sw, 0) == 0);
        }
        if (i == 0) {
            assert(strncmp(f->buf, "oa", 2) == 0);
        } else if (i == 1) {
            assert(strncmp(f->buf, "oat ", 4) == 0);
        } else if (i == 2) {
            assert(strncmp(f->buf, "oat -a", 6) == 0);
        } else if (i == 3) {
            assert(strncmp(f->buf, "oat -a -", 8) == 0);
        }
        free(mock_read_buf);
    }
    assert(f->sr->eof == 0);
    return 0;
}


void teardown(Fixture* f) {
    // Now just the cleanup
    StreamReader_destroy(f->sr);
    StreamWriter_destroy(f->sw);
    free(f->parse_ctx);
    free(f->request_read_ctx);
    free(f->buf);
    free(f);
    test_out("Reading and Writing complete");
}


int main(int argc, char** argv) {
    assert(argc == 1);
    Fixture* fixture;

    // Let's start at the front of the buffer any time the parse exactly matches the read
    test_out("\nConsecutive Successful Read and Parse\n=====================\n");
    fixture = setup();
    assert(test_consecutive_successful_read_and_parse(fixture) == 0);
    teardown(fixture);
    test_out("PASSED\n\n");

    test_out("\nParser Partial Read\n===============\n");
    // The parser might return some of the bytes. The next parse should happen half-way
    fixture = setup();
    assert(test_parser_partial_read(fixture) == 0);
    teardown(fixture);
    test_out("PASSED\n\n");

    // Continue the buffer when there isn't enough data
    test_out("\nParser Increments\n===============\n");
    fixture = setup();
    assert(test_parser_increments(fixture) == 0);
    teardown(fixture);
    test_out("PASSED\n\n");

    // Fail when the buffer gets full
    test_out("\nFull Buffer\n===============\n");
    fixture = setup();
    assert(test_full_buffer(fixture) == 0);
    teardown(fixture);
    test_out("PASSED\n\n");

    // Stay EOF when the file closes
    test_out("\nEOF Read\n=====================\n");
    fixture = setup();
    assert(test_eof_read(fixture) == 0);
    teardown(fixture);
    test_out("PASSED\n\n");

    // (Internally, some of the buffer could have been for the headers, some
    //  for the body, the streamer doesn't need to know, the parse_ctx can
    //  keep track of that for the next parse)
    test_out("\nParser Overflow\n===============\n");
    // Fail when the parser claims more data than was read into the buffer
    fixture = setup();
    assert(test_parser_overflow(fixture) == 0);
    teardown(fixture);
    test_out("PASSED\n\n");

    test_out("\nParser Bad Error\n===============\n");
    // Fail if the parser returns something other than 0, or a length of bytes
    fixture = setup();
    assert(test_parser_bad_error(fixture) == 0);
    teardown(fixture);
    test_out("PASSED\n\n");

    // Check nothing has changed the values of dummy variables
    assert(dummy_in_fd == -1);
    assert(dummy_out_fd == -2);
    return 0;
}
