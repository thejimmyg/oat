#ifndef __james_h__
#define __james_h__

#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <string.h>

#ifdef DEBUG

#define debug(M, ...) fprintf(stderr, "DEBUG %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define test_out(M, ...) fprintf(stderr, "" M "\n", ##__VA_ARGS__)
#define log_warn(M, ...) fprintf(stderr, "[WARN] " M "\n", ##__VA_ARGS__)
#define log_info(M, ...) fprintf(stderr, "[INFO] " M "\n", ##__VA_ARGS__)
#define log_debug(M, ...) fprintf(stderr, "[DEBUG] " M "\n", ##__VA_ARGS__)

#else

#define debug(M, ...)
#define test_out(M, ...)
#define log_warn(M, ...)
#define log_info(M, ...)
#define log_debug(M, ...)

#endif /* DEBUG */


#define log_error(M, ...) fprintf(stderr, "[ERROR] " M "\n", ##__VA_ARGS__)
//#define check(A, M, ...) if(!(A)) { log_error(M, ##__VA_ARGS__); errno=0; goto error; }
#define check_mem(A) check((A), "Out of memory.")
// #define expect(A, M, ...) if(!(A)) { log_error(M, ##__VA_ARGS__); }

#endif /* __james_h__ */

