#!/bin/bash

if [ -e ragelcalc.c ]; then
    rm ragelcalc.c
fi
make ragelcalc.c
if [ -e autodemo ]; then
    rm -r autodemo
fi
mkdir -p autodemo
cd autodemo
cat > Makefile.am << EOF
bin_PROGRAMS=ragelcalc
ragelcalc_SOURCES=ragelcalc.c
ragelcalc_CFLAGS=-Wall -Wextra -std=c99 -O3 -g

EOF

cp ../ragelcalc.c .
cp ../ragelcalc.rl .
autoscan
sed -e 's/FULL-PACKAGE-NAME/ragelcalc/' \
    -e 's/VERSION/1/' \
    -e 's|BUG-REPORT-ADDRESS|/dev/null|' \
    -e '10i\
AM_INIT_AUTOMAKE' < configure.scan > configure.ac

# AC_PROG_CC
touch NEWS README AUTHORS ChangeLog

autoreconf -iv
./configure
make distcheck
cd ../

