#include "streamer.h"
#include "james.h"
#include "cmd_parser.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>
#include <assert.h>

#define STRATEGY_NONE   0
#define STRATEGY_EOF    1
#define STRATEGY_LENGTH 2

Streamer* Streamer_create(Ctx *ctx, strategy_cb strategy_cb, data_cb data_cb, end_cb end_cb, size_t BUF_LEN) {
    Streamer *parser = (Streamer *)malloc(sizeof(Streamer));
    CmdParseResult* cmd_parse_result;
    return parser;

}


long Streamer_parse(Streamer* parser, const char* p, int len, int is_eof )
{
    return -2;
}


int Streamer_on_data_available(int fd, Streamer *parser)
{
    int len_read;
    len_read = read(fd, &parser->real_buf[parser->marked_len], parser->BUF_LEN-parser->marked_len);
    int consumed_this_read = 0;
    debug("Read up to %lu more bytes. Got %d ...", parser->BUF_LEN-parser->marked_len, len_read);
    if (len_read == -1) {
        perror("read");
        return -1;
    } else if (len_read == 0) {
        // Send an EOF in case it is needed
        debug("No more data read");
        assert(parser->real_buf[parser->BUF_LEN] == '\0');
        int consumed = Streamer_parse(parser, &parser->real_buf[parser->BUF_LEN], 0, 1);
        parser->consumed_so_far += consumed;
        debug("EOF - Consumed: %d", consumed);
        return 0;
    } else {
        /* Set the value after the end of the buffer to '\0' so that pe is always '\0' */
        assert(parser->marked_len+len_read <= parser->BUF_LEN);
        parser->real_buf[parser->marked_len+len_read] = '\0';
        debug("Buf: -->%s<--", &parser->real_buf[parser->marked_len]);
        assert(strlen(&parser->real_buf[parser->marked_len]) == len_read);
        while (consumed_this_read < len_read) {
            int available = len_read - consumed_this_read;
            if (parser->first) {
                debug("Start parsing - Available: %d", available);
                parser->first = 0;
            } else {
                debug("Continue parsing from %d - Available: %d", parser->marked_len, available);
            }
            consumed_this_read += Streamer_parse(
                parser,
                &parser->real_buf[parser->marked_len+consumed_this_read],
                available,
                0
            );
            debug("End parsing for this parse loop - Consumed: %d, remaining buffer: %d", consumed_this_read, len_read-consumed_this_read);
        }

        parser->consumed_so_far += consumed_this_read;
        // Make sure whatever is on the other side gets the data, even when it
        // is a pipe to a non-tty such as a program
        // http://stackoverflow.com/questions/13932932/why-does-stdout-need-explicit-flushing-when-redirected-to-file
        fflush(stdout);

        return len_read;
    }
}

void Streamer_destroy(Streamer *parser) {
    free(parser);
}

int run(void) {
        InitCtx initctx;
        initctx.super.type = INIT_CTX_TYPE;
        initctx.queue_out = Queue_create();
        Streamer *parser = Streamer_create(&initctx.super, &on_strategy, &on_data, &on_end, BUF_LEN);
        debug("Max length: %d",  BUF_LEN);

        struct timeval timeout;
        fd_set fdread, fdwrite;
        int timeoutms;
        int ret, res, len, write_len;
        int maxfd;
        res = 1;
        while(res > 0 || initctx.queue_out->front != NULL) {
            timeoutms = 1000000; /* With this set to 0, things behave strangely with pipes */
            maxfd = -1;
            timeout.tv_sec = 0;
            timeout.tv_usec = 0;
            /* Wait on stdin for input */
            FD_ZERO(&fdread);
            FD_ZERO(&fdwrite);
            FD_SET(0, &fdread);
            if (0 > maxfd) {
                maxfd = 0;
            }
            if (initctx.queue_out->front != NULL) {
                 log_info("Got some data to write to stdout");
                 FD_SET(1, &fdwrite);
                 if (1 > maxfd) {
                     maxfd = 1;
                 }
            }
            if (timeoutms >= 0) {
                timeout.tv_sec = timeoutms / 1000;
                if (timeout.tv_sec > 1) {
                    /* Let's not give the plugins too much control */
                    timeout.tv_sec = 1;
                } else {
                    timeout.tv_usec = (timeoutms % 1000) * 1000;
                }
            }
            ret = select(maxfd + 1, &fdread, &fdwrite, NULL, &timeout);
            if (ret == -1){
                log_error("select");
                break;
            } else if ( ret == 0 ) {
                //debug("%d milliseconds elapsed.", timeoutms);
            } else {
                if (FD_ISSET(0, &fdread)) {
                    res = Streamer_on_data_available(STDIN_FILENO, parser);
                }
                if (FD_ISSET(1, &fdwrite)) {
                    Chunk *chunk = initctx.queue_out->front;
                    write_len = chunk->len - chunk->written;
                    len = write(1, &chunk->buf[chunk->written], write_len);
                    if (len <= 0) {
                        /* "Error during write" */
                        exit(5);
                    }
                    log_info("Written %d bytes", len);
                    chunk->written = chunk->written + len;
                    if (chunk->written == chunk->len) {
                            Queue_delete(initctx.queue_out, &chunk);
                            Chunk_destroy(chunk);
                            //chunk->callback(0, chunk->context);
                    }
                }
            }
        }
        Queue_destroy(initctx.queue_out);
        initctx.queue_out = NULL;
        Streamer_destroy(parser);
        parser = NULL;
        debug("Freed queue_out and oat_parser");
    }
}
