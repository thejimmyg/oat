#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BUF_LEN 1024 /* Read buffer in bytes */

/* This is based on 

https://github.com/PowerDNS/pdns/blob/master/pdns/dnslabeltext.rl

So therefore we add:

    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

See also:
* The oat example which actually does this via ** in a more complicated case
* Section 6.5 of the Ragel manual about semantic conditions here: http://www.colm.net/files/ragel/ragel-guide-6.9.pdf
* http://www.complang.org/pipermail/ragel-users/2010-July/002427.html

*/


typedef struct {
        uint8_t cs;
        long val;
        bool success;
        bool neg;
} escape_parser;

%%{
        machine escape;
        access parser->;

        action reportEscaped {
          // char c = *fpc;
          // appendSplit(ret, segment, c);
        }
        action reportEscapedNumber {
          // char c = *fpc;
          // val *= 10;
          // val += c-'0';
          
        }
        action doneEscapedNumber {
          // appendSplit(ret, segment, val);
          // val=0;
        }
        
        action reportPlain {
          // appendSplit(ret, segment, *(fpc));
        }

        action end {
          parser->success = true;
        }

        escaped = '\\' (([^0-9]@reportEscaped) | ([0-9]{3}$reportEscapedNumber%doneEscapedNumber));
        plain = ((any-'\\'-'"')) $ reportPlain;
        txtElement = escaped | plain;
        
        token = ('"' txtElement* '"' space?) ( '\n' )? %end;
        main := token;

}%%

%% write data;

void init_escape_parser(escape_parser* parser)
{
        %% write init;
}

void parse_escape(escape_parser* parser, const char* p, uint16_t len, uint8_t is_eof )
{
        /* pe points to 1 byte beyond the end of this block of data */
        const char* pe = p + len;
        /* Indicates the end of all data, 0 if not in this block */
        const char* eof = is_eof ? pe : ((char*) 0);
        %% write exec;
}


int main(int argc, char** argv)
{
        if (argc != 1) {
                fprintf(stderr, "Usage: ragel\n");
                return 1;
        }
        escape_parser parser;
        parser.success = false;
        parser.val = 0;
        parser.neg = false;
        init_escape_parser(&parser);
        char buf[BUF_LEN+1];
        int len;
        len = read(STDIN_FILENO, buf, sizeof(buf));
        if (len == -1) {
                perror("read");
                return 1;
        } else if (len == 0) {
                fprintf(stderr, "EOF\n");
        } else {
                buf[len] = '\0';
                parse_escape(&parser, buf, len, 0);
                parse_escape(&parser, buf, 0, 1);
        }
        if ( ! parser.success ) {
                fprintf( stderr, "escape: there was an error\n" );
                return 1;
        }
        if ( parser.neg ) {
                parser.val = -1 * parser.val;
        }
        printf("Result: %ld\n", parser.val);
        return 0;
}
