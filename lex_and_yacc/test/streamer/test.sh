#!/bin/sh

set -e


RESULT=$( ./debug/streamer_test  2>&1 )
CODE=$?
printf "$RESULT" > test/streamer/actual
if [ ! x"$CODE" = x"0" ]; then
    printf "Error -->${RESULT}<--\n"
    exit 1
fi

if ! diff -u test/streamer/expected test/streamer/actual; then
    printf "Failed the test"
    exit 2
else
    printf "streamer - Success\n"
fi

rm test/streamer/select_actual_out
python test/streamer/gen.py $MAX > test/streamer/in 2> test/streamer/select_expected_out

time ./test/streamer/select.sh
CODE=$?
if [ ! x"$CODE" = x"0" ]; then
    printf "Error -->${RESULT}<--\n"
    exit 3
fi

if ! diff -u test/streamer/select_expected_out test/streamer/select_actual_out; then
    printf "Failed streamer select test"
    exit 4
else
    printf "streamer select - Success\n"
fi

