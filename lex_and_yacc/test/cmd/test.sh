#!/bin/sh

set -e


RESULT=$( ./debug/cmd_test 2>&1 )
CODE=$?
printf "$RESULT" > test/cmd/actual
if [ ! x"$CODE" = x"0" ]; then
    printf "Error -->${RESULT}<--\n"
    exit 1
fi

EXPECTED=$(cat test/cmd/expected)
if [ ! x"$RESULT" = x"$EXPECTED" ]; then
    diff -u test/cmd/expected test/cmd/actual
    printf "Failed the test"
    exit 2
else
    printf "cmd - Success\n"
fi
