#include "james.h"
#include "cmd.h"

#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <stdio.h>
#include <errno.h>
#include <assert.h>


ArgList* ArgList_create() {
    ArgList* a = (ArgList*)malloc(sizeof(ArgList));
    a->array = (const char **)malloc(1 * sizeof(char *));
    a->item_sizes = (size_t *)malloc(1 * sizeof(size_t));;
    a->used = 0;
    a->size = 1;
    return a;
}

void ArgList_append(ArgList *a, const char* element, size_t len) {
    assert(element[len] == '\0');
    if (a->used == a->size) {
        /* Args is already one long */
        if (a->size >= 1) {
            a->size *= 2;
        }
        test_out("Reallocating array to %zu", a->size);
        a->array = (const char **)realloc(a->array, a->size * sizeof(char *));
        a->item_sizes = (size_t *)realloc(a->item_sizes, a->size * sizeof(size_t));
    }
    test_out("Appending -->%.*s<-- as element %zu", (int)len, element, a->used);
    a->array[a->used] = element;
    a->item_sizes[a->used] = len;
    a->used = a->used + 1;
}

void ArgList_destroy(ArgList *a) {
    int x;
    for (x=0; x<a->used; x++) {
        free((void *)a->array[x]);
    }
    free(a->array);
    a->array=NULL;
    free(a->item_sizes);
    a->item_sizes=NULL;
    free(a);
}

char* ArgList_to_string(ArgList *a) {
    int i, j;
    int size = 0;
    for (i=0; i<a->used; i++) {
        size += a->item_sizes[i] + 1; // For the space after
    }
    char* buf = (char *) malloc(sizeof(char) * size);
    size = 0;
    for (i=0; i<a->used; i++) {
        for (j=0; j<a->item_sizes[i]; j++) {
            buf[size] = a->array[i][j];
            size = size + 1;
        }
        buf[size] = ' ';
        size++;
    }
    // Replace the final space with a null.
    buf[size-1] = '\0';
    return buf;
}


Cmd* Cmd_create() {
    Cmd* c = (Cmd *)malloc(sizeof(Cmd));
    c->name_len = 0;
    c->name = NULL;
    c->args = ArgList_create();
    return c;
}

void Cmd_destroy(Cmd *cmd) {
    ArgList_destroy(cmd->args);
    cmd->args = NULL;
    free(cmd->name);
    free(cmd);
};

