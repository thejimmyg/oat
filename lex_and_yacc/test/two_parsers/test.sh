#!/bin/sh

set -e


RESULT=$( ./debug/two_parsers_test 2> /dev/null )
CODE=$?
echo "$RESULT" > test/two_parsers/actual
if [ ! x"$CODE" = x"0" ]; then
    printf "Error -->${RESULT}<--\n"
    exit 1
fi

if ! diff -u test/two_parsers/expected test/two_parsers/actual; then
    printf "Failed the test"
    exit 2
else
    printf "two_parsers - Success\n"
fi
