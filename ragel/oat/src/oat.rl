#include "./oat.h"
#include "vnd/zed/dbg.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>
#include <assert.h>


#define STRATEGY_NONE   0
#define STRATEGY_EOF    1
#define STRATEGY_LENGTH 2

#define GET_LEN(FPC) ((FPC) - parser->mark_pos)


ArgList* ArgList_create() {
    ArgList* a = (ArgList*)malloc(sizeof(ArgList));
    a->array = (const char **)malloc(1 * sizeof(char *));
    a->item_sizes = (size_t *)malloc(1 * sizeof(size_t));;
    a->used = 0;
    a->size = 1;
    return a;
}

void ArgList_append(ArgList *a, const char* element, size_t len) {
    assert(element[len] == '\0');
    if (a->used == a->size) {
        /* Args is already one long */
        if (a->size >= 1) {
            a->size *= 2;
        }
        debug("Reallocating array to %zu", a->size);
        a->array = (const char **)realloc(a->array, a->size * sizeof(char *));
        a->item_sizes = (size_t *)realloc(a->item_sizes, a->size * sizeof(size_t));
    }
    debug("Appending -->%.*s<-- as element %zu", (int)len, element, a->used);
    a->array[a->used] = element;
    a->item_sizes[a->used] = len;
    a->used = a->used + 1;
}

void ArgList_destroy(ArgList *a) {
    int x;
    for (x=0; x<a->used; x++) {
        free((void *)a->array[x]);
    }
    free(a->array);
    a->array=NULL;
    free(a->item_sizes);
    a->item_sizes=NULL;
    free(a);
}


Cmd* Cmd_create() {
    Cmd* c = (Cmd *)malloc(sizeof(Cmd));
    c->name_len = -1;
    c->name = NULL;
    c->args = ArgList_create();
    return c;
}


void Cmd_destroy(Cmd *c) {
    ArgList_destroy(c->args);
    c->args = NULL;
    free(c->name);
    free(c);
};



void string(char* name, const char* p, OatParser* parser) {
    int len = p-(parser->mark_pos);
    log_info("%s: (%d-%d): %.*s", name, parser->consumed_so_far-parser->marked_len, parser->consumed_so_far+len-parser->marked_len, len, parser->mark_pos);
};


%%{
    machine oat;
    access parser->;

    action mark {
        parser->mark_pos = p;
        debug("Mark: (%ld)", parser->mark_pos - parser->real_buf + parser->consumed_so_far-parser->marked_len);
    }

    action unmark {
        debug("Unmark");
        parser->mark_pos = NULL;
    }

    action base_name {
        string("Base name", p, parser);
        debug("Unmark");
        parser->mark_pos = NULL;
    }

    action sub_cmd_name {
        string("Sub command name", p, parser);
        int len = p - parser->mark_pos;
        parser->cmd->name = malloc((len+1)*(sizeof(char)));
        strncpy(parser->cmd->name, parser->mark_pos, len);
        parser->cmd->name[len] = '\0';
        parser->cmd->name_len = len;
        log_info("Name: -->%s<-- len %ld", parser->cmd->name, parser->cmd->name_len);
        debug("Unmark");
        parser->mark_pos = NULL;

    }

    action short_option {
        string("Short option", p, parser);

        size_t len = GET_LEN(p);
        char* opt = (char *)malloc((len+2)*(sizeof(char)));
        opt[0] = '-';
        strncpy(opt+1, parser->mark_pos, len+1);
        opt[len+1] = '\0';
        ArgList_append(parser->cmd->args, (const char *)opt, len+1);

        debug("Unmark");
        parser->mark_pos = NULL;
    }

    action long_option {
        string("Long option", p, parser);

        size_t len = GET_LEN(p);
        char* opt = (char *)malloc((len+3)*(sizeof(char)));
        opt[0] = '-';
        opt[1] = '-';
        strncpy(opt+2, parser->mark_pos, len+2);
        opt[len+2] = '\0';
        ArgList_append(parser->cmd->args, (const char *)opt, len+2);

        debug("Unmark");
        parser->mark_pos = NULL;
    }

    action unquoted_argument {
        string("Unquoted argument", p, parser);

        size_t len = GET_LEN(p);
        char* opt = (char *)malloc((len+1)*(sizeof(char)));
        strncpy(opt, parser->mark_pos, len);
        opt[len] = '\0';
        ArgList_append(parser->cmd->args, (const char *)opt, len);

        debug("Unmark");
        parser->mark_pos = NULL;
    }

    action quoted_argument {
        string("Quoted argument", p, parser);

        size_t len = GET_LEN(p);
        char* opt = (char *)malloc((len-1)*(sizeof(char)));
        int x;
        int y=0;
        for (x=0; x<len-2; x++) {
            // debug("Char %s", parser->mark_pos+1+x);
            if ((parser->mark_pos+1)[x] == '"') {
                assert((parser->mark_pos+1)[x+1] == '"');
                opt[y] = (parser->mark_pos+1)[x];
                x++;
            } else {
                opt[y] = (parser->mark_pos+1)[x];
            }
            y++;
        }
        int offset = x-y;
        opt[len-offset-2] = '\0';
        debug("Removed %d character(s) when unquoting", offset);
        ArgList_append(parser->cmd->args, (const char *)opt, len-offset-2);

        debug("Unmark");
        parser->mark_pos = NULL;
    }

    action strategy_none {
        debug("Strategy: None");
        if (parser->strategy != NULL)
            parser->strategy(parser->ctx, parser->cmd, STRATEGY_NONE, -1);
    }

    action strategy_eof {
        debug("Strategy: EOF");
        if (parser->strategy != NULL)
            parser->strategy(parser->ctx, parser->cmd, STRATEGY_EOF, -1);
    }

    action strategy_length {
        debug("Strategy: Length");
        if (parser->strategy != NULL)
            parser->strategy(parser->ctx, parser->cmd, STRATEGY_LENGTH, parser->datalen);
    }

    action stream_data {
        debug("Stream data: %c", fc);
        if (parser->data)
            parser->data(parser->ctx, parser->cmd, p);
    }

    action test_len {
        parser->datalen_charcount++ < parser->datalen
    }

    action start_num {
        debug("Start num charcount");
        parser->datalen_charcount = 0;
        parser->datalen = 0;
        debug("End start num");
    }

    action length {
        // debug("End num: %c %p %p %p", fc, parser->mark_pos, p, pe);
        /* To distinguish success/failure after call */
        errno = 0;
        parser->datalen = strtol(parser->mark_pos, (char **) &p, 10);
        /* Check for various possible errors */
        if ((errno == ERANGE && (parser->datalen == LONG_MAX || parser->datalen == LONG_MIN)) || (errno != 0 && parser->datalen == 0)) {
            perror("strtol");
            exit(EXIT_FAILURE);
        }
        if (p == pe) {
            log_err("No digits were found");
            exit(EXIT_FAILURE);
        }
        if (parser->datalen < 0){
            log_err("Can't have a data length less than zero");
            exit(EXIT_FAILURE);
        }
        /* If we got here, strtol() successfully parsed a number */
        debug("Parsed int: %lu", parser->datalen);
        if (parser->length != NULL)
            parser->length(parser->cmd, parser->mark_pos, GET_LEN(p));
        string("Length", p, parser);
        debug("Unmark");
        parser->mark_pos = NULL;
    }

    action end {
        debug("end");
        if (parser->end != NULL)
            parser->end(parser->ctx, parser->cmd);
        OatParser_reset(parser);
    }

    base_name = 'oat' >mark %base_name;
    sub_cmd_name = [a-z]+ >mark %sub_cmd_name;

    short_option = '-' [a-zA-Z]+  >mark %short_option;
    long_option = '--' [a-zA-Z]+  >mark %long_option;
    unquoted_argument = ([a-zA-Z0-9][a-zA-Z0-9_/\[\]\{\}!@:|\\;$%^&*()\.\-]+) >mark %unquoted_argument;
    escaped_quote = '""';
    quoted_value = ([^"]+ | escaped_quote+)*;
    quoted_argument = '"' quoted_value :>> '"';

    options = (short_option | long_option | unquoted_argument | quoted_argument+ >mark %quoted_argument);
    length = digit+ >mark >start_num %length;
    streamer = '<<';
    base_cmd = base_name [ ]+ sub_cmd_name ([ ]+ options)*;
    strategy_none   = base_cmd [ ]? %strategy_none %end;
    strategy_eof    = base_cmd [ ]? streamer [ ]? 'EOF' [ ]? '\n' %strategy_eof;
    strategy_length = base_cmd [ ]+ streamer [ ]? length [ ]? '\n' %strategy_length;
    data = (any when test_len $stream_data)*;
    cmd = ( strategy_none | strategy_eof (any @stream_data)* :> '\nEOF' %end | strategy_length data %end);
    main := ( cmd '\n'+)** $!{ log_err("ERR:>%s<", pe); exit(9); };

}%%


%% write data;


void OatParser_reset(OatParser *parser){
    if (parser->cmd != NULL) {
        Cmd_destroy(parser->cmd);
    }
    parser->cmd = Cmd_create();
    parser->first = 1;
    parser->marked_len = 0;
    parser->mark_pos = NULL;
    parser->consumed_so_far = 0;
    //parser->sub_cmd_name = NULL;
    parser->length = NULL;
    assert(parser->real_buf[parser->BUF_LEN] == '\0');
}


OatParser* OatParser_create(Ctx *ctx, strategy_cb strategy_cb, data_cb data_cb, end_cb end_cb, size_t BUF_LEN)
{

    OatParser *parser = (OatParser *)malloc(sizeof(OatParser));

    parser->BUF_LEN = BUF_LEN;
    parser->ctx = ctx;

    assert(strategy_cb != NULL);
    assert(data_cb != NULL);
    assert(end_cb != NULL);
    parser->data = data_cb;
    parser->strategy = strategy_cb;
    parser->end = end_cb;

    /* Must be set so that OatParser_reset() below doesn't try to destory it */
    parser->cmd = NULL;
    /* Allocate one more than the buffer size so that we can put a null char at the end */
    parser->real_buf = (char *) malloc(sizeof(char)*(parser->BUF_LEN+1));
    parser->real_buf[parser->BUF_LEN] = '\0';

    /* Save a moving pointer to the current start of the buffer */
    // parser->buf = parser->real_buf;
    debug("Buffer size: %lu", sizeof(parser->real_buf));

    OatParser_reset(parser);

    %% write init;

    return parser;
}


long OatParser_parse(OatParser* parser, const char* p, int len, int is_eof )
{
    assert(len <= parser->BUF_LEN);
    /* pe points to 1 byte beyond the end of this block of data which we
       allocated when creating real_buf */
    const char* pe = p + len;
    assert(*pe == '\0');
    const char* p_start = p;
    /* Indicates the end of all data, 0 if not in this block */
    const char* eof = is_eof ? pe : NULL;
    %% write exec;

    /* The consumed value is always at least one character more than the end
       because of the \n after the cmd definition in main: above. Let's return
       the current position, compared to where we were told the start of the
       buffer was. This can never be more than the length of the buffer
       remaining.
    */
    int consumed = p-p_start;
    assert(consumed <= len);
    assert(consumed >= 0);
    return consumed;
}


int OatParser_on_data_available(int fd, OatParser *parser)
{
    // Need to keep any information from start-pos -> end in the buffer.
    if (parser->mark_pos != NULL) {
        parser->marked_len = parser->real_buf+parser->BUF_LEN - parser->mark_pos;
        /* We need the marked data to be put at the front of the new buffer */
        debug("Copying %d bytes of the string to the start of the buffer and adjusting the mark: -->%.*s<--", parser->marked_len, parser->marked_len, parser->mark_pos);
        memmove(parser->real_buf, parser->mark_pos, parser->marked_len);
        if (parser->BUF_LEN-parser->marked_len < 1) {
            log_err("ERROR: No free space in the buffer to parse the command - perhaps an argument is too long?");
            exit(8);
        }
        parser->mark_pos = &parser->real_buf[0];
        // parser->buf = &parser->real_buf[0];
    } else {
        parser->marked_len = 0;
    }

    int len_read;

    len_read = read(fd, &parser->real_buf[parser->marked_len], parser->BUF_LEN-parser->marked_len);
    int consumed_this_read = 0;
    debug("Read up to %lu more bytes. Got %d ...", parser->BUF_LEN-parser->marked_len, len_read);
    if (len_read == -1) {
        perror("read");
        return -1;
    } else if (len_read == 0) {
        // Send an EOF in case it is needed
        debug("No more data read");
        assert(parser->real_buf[parser->BUF_LEN] == '\0');
        int consumed = OatParser_parse(parser, &parser->real_buf[parser->BUF_LEN], 0, 1);
        parser->consumed_so_far += consumed;
        debug("EOF - Consumed: %d", consumed);
        return 0;
    } else {
        /* Set the value after the end of the buffer to '\0' so that pe is always '\0' */
        assert(parser->marked_len+len_read <= parser->BUF_LEN);
        parser->real_buf[parser->marked_len+len_read] = '\0';
        debug("Buf: -->%s<--", &parser->real_buf[parser->marked_len]);
        assert(strlen(&parser->real_buf[parser->marked_len]) == len_read);
        while (consumed_this_read < len_read) {
            int available = len_read - consumed_this_read;
            if (parser->first) {
                debug("Start parsing - Available: %d", available);
                parser->first = 0;
            } else {
                debug("Continue parsing from %d - Available: %d", parser->marked_len, available);
            }
            consumed_this_read += OatParser_parse(
                parser,
                &parser->real_buf[parser->marked_len+consumed_this_read],
                available,
                0
            );
            debug("End parsing for this parse loop - Consumed: %d, remaining buffer: %d", consumed_this_read, len_read-consumed_this_read);
        }

        parser->consumed_so_far += consumed_this_read;
        // Make sure whatever is on the other side gets the data, even when it
        // is a pipe to a non-tty such as a program
        // http://stackoverflow.com/questions/13932932/why-does-stdout-need-explicit-flushing-when-redirected-to-file
        fflush(stdout);

        return len_read;
    }
}


void OatParser_destroy(OatParser *parser) {
    free(parser->real_buf);
    parser->real_buf = NULL;
    if (parser->cmd != NULL) {
        Cmd_destroy(parser->cmd);
        parser->cmd = NULL;
    }
    free(parser);
}

