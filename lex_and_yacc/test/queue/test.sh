#!/bin/sh

set -e


RESULT=$( ./debug/queue_test 2>&1 )
CODE=$?
printf "$RESULT" > test/queue/actual
if [ ! x"$CODE" = x"0" ]; then
    printf "Error -->${RESULT}<--\n"
    exit 1
fi

EXPECTED=$(cat test/queue/expected)
if [ ! x"$RESULT" = x"$EXPECTED" ]; then
    diff -u test/queue/expected test/queue/actual
    printf "Failed the test"
    exit 2
else
    printf "queue - Success\n"
fi
