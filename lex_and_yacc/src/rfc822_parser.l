%{
#include <stdio.h>
#include <string.h>
#include "rfc822_parser.y.h"
#include "rfc822_parser.h"

%}

%option noyywrap

%%

[ ]+                        { rfc822p->next_char=yy_hold_char; return TOKSPACE; }
":"                         { rfc822p->next_char=yy_hold_char; return TOKSEPARATOR; }
[\n]+                       { rfc822p->next_char=yy_hold_char; return TOKNEWLINE; }
[a-zA-z0-9]+                { rfc822p->next_char=yy_hold_char; rfc822_parser_yylval.string=strdup(yytext); return STRING; }
.                           { rfc822p->next_char=yy_hold_char; rfc822_parser_yyerror("unexpected character"); return ERROR; }
%%
