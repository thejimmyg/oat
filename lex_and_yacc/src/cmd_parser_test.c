#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "cmd_parser.h"
#include "james.h"


typedef struct Fixture Fixture;
struct Fixture {
    char* buf;
    size_t len;
};


void dump(char* buf, size_t len) {
    size_t i;
    for (i=0; i<len; i++) {
        if (buf[i] == '\n') {
            printf("\\n");
        } else if (buf[i] == '\0') {
            printf("N");
        } else {
            printf("%c", buf[i]);
        } 
    }
}


Fixture* setup() {
    Fixture* fixture = (Fixture *) malloc (sizeof(Fixture));
    char* buf = (char *) malloc (sizeof(char) * 14);
    memset(buf,'$', 14);
    fixture->buf = buf;
    fixture->len = 14;
    return fixture;
}


void teardown(Fixture* fixture) {
    free(fixture->buf);
    free(fixture);
}


int test_lex_error(Fixture* f) {
    char* in = f->buf;
                  //0123456789
    memcpy(f->buf, "oat i+ -a\n", 10);
    CmdParser_expect(in, f->buf);
    CmdParser* cmd_parser = CmdParser_create(f->buf, 14, 10);
    printf("Res: >"); dump(f->buf, 14); printf("<\n");
    CmdParser_expect(cmd_parser->res, 1);
    CmdParser_expect(cmd_parser->cur_offset, 5);
    CmdParser_expect(f->buf[cmd_parser->cur_offset], '+');
    CmdParser_expect(memcmp(f->buf, "oat i+ -a\n$$\0\0", 14), 0);
    return 0;
}


int test_yacc_error(Fixture* f) {
    char* in = f->buf;
                  //0123456789
    memcpy(f->buf, "oat !    \n", 10);
    CmdParser_expect(in, f->buf);
    CmdParser* cmd_parser = CmdParser_create(f->buf, 14, 10);
    printf("Res: >"); dump(f->buf, 14); printf("<\n");
    CmdParser_expect(cmd_parser->res, 1);
    CmdParser_expect(cmd_parser->cur_offset, 4);
    CmdParser_expect(f->buf[cmd_parser->cur_offset], '!');
    CmdParser_expect(memcmp(f->buf, "oat !    \n$$\0\0", 14), 0);
    return 0;
}


int test_success(Fixture* f) {
    char* in = f->buf;
                  //0123456789
    memcpy(f->buf, "oat in -a\n", 10);
    CmdParser_expect(in, f->buf);
    printf("Res: >"); dump(f->buf, 14); printf("<\n");
    CmdParser* cmd_parser = CmdParser_create(f->buf, 14, 10);
    printf("Res: >"); dump(f->buf, 14); printf("<\n");
    CmdParser_expect(cmd_parser->res, 0);
    CmdParser_expect(cmd_parser->cur_offset, 9);
    CmdParser_expect(f->buf[cmd_parser->cur_offset], '\n');
    CmdParser_expect(memcmp(f->buf, "oat in -a\n$$\0\0", 14), 0);
    char* args = ArgList_to_string(cmd_parser->cmd->args);
    printf("Cmd: %s %s\n", cmd_parser->cmd->name, args);
    CmdParser_expect(memcmp(cmd_parser->cmd->name, "oat", 4), 0);
    CmdParser_expect(memcmp(args, "in -a", 5), 0);
    free(args);
    return 0;
}


int test_end_of_input(Fixture* f) {
    char* in = f->buf;
                  //0123456789012
    memcpy(f->buf, "oat in a  -c", 12);
    CmdParser_expect(in, f->buf);
    CmdParser* cmd_parser = CmdParser_create(f->buf, 14, 10);
    printf("Res: >"); dump(f->buf, 14); printf("<\n");
    CmdParser_expect(cmd_parser->res, 1);
    CmdParser_expect(cmd_parser->cur_offset, 12);
    CmdParser_expect(f->buf[cmd_parser->cur_offset], '\0');
    CmdParser_expect(memcmp(f->buf, "oat in a  -c\0\0", 14), 0);
    return 0;
}


int test_streaming(Fixture* f) {
    char* in = f->buf;
                 //0123456 789012 
    memcpy(f->buf, "oat a\noat b\n", 12);
    CmdParser_expect(in, f->buf);
    CmdParser* cmd_parser = CmdParser_create(f->buf, 14, 10);
    printf("Res: >"); dump(f->buf, 14); printf("<\n");
    CmdParser_expect(cmd_parser->res, 0);
    CmdParser_expect(cmd_parser->cur_offset, 5);
    CmdParser_expect(memcmp(f->buf, "oat a\noat b\n\0\0", 14), 0);
    char* args = ArgList_to_string(cmd_parser->cmd->args);
    printf("Cmd: %s %s\n", cmd_parser->cmd->name, args);
    CmdParser_expect(memcmp(cmd_parser->cmd->name, "oat", 4), 0);
    CmdParser_expect(memcmp(args, "a", 2), 0);
    free(args);
    return 0;
}


int main(){

    Fixture* f;

    printf("\nLex Error Test\n==============\n\n");
    f = setup();
    test_lex_error(f);
    teardown(f);
    printf("PASSED\n\n");

    printf("Yacc Error Test\n===============\n\n");
    f = setup();
    test_yacc_error(f);
    teardown(f);
    printf("PASSED\n\n");

    printf("Success Test\n============\n\n");
    f = setup();
    test_success(f);
    teardown(f);
    printf("PASSED\n\n");

    printf("End of input Test\n=================\n\n");
    f = setup();
    test_end_of_input(f);
    teardown(f);
    printf("PASSED\n\n");

    printf("Streaming Test\n==============\n\n");
    f = setup();
    test_streaming(f);
    teardown(f);
    printf("PASSED\n\n");

    return 0;
}
