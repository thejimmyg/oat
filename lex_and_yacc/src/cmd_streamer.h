#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include "cmd.h"

typedef struct {
    int type;
} Ctx;


typedef void (*strategy_cb)(Ctx *ctx, Cmd *cmd, int strategy, size_t length);
typedef void (*data_cb)(Ctx *ctx, Cmd *cmd, const char* byte);
typedef void (*end_cb)(Ctx *ctx, Cmd *cmd);
typedef void (*cmd_cb)(Cmd *cmd, const char *string, int length);

typedef struct {
    /* Custom parser variables */
    Cmd *cmd;
    Ctx *ctx;
    char* real_buf;
    // We would need one per overlapping value, but we don't have any luckily, so we share this one.
    size_t BUF_LEN;
    const char* mark_pos;
    int marked_len;
    int consumed_so_far;
    int first;
    // For iterating through the length of data
    long datalen;
    long datalen_charcount;
    /* Ragel variables */
    uint8_t cs;
    // For handling the fcall for parsing quotedvalue
    // int top;
    // int stack[];

    //cmd_cb sub_cmd_name;
    // cmd_cb short_option;
    // cmd_cb long_option;
    // cmd_cb unquoted_argument;
    // cmd_cb quoted_argument;
    cmd_cb length;
    strategy_cb strategy;
    data_cb data;
    end_cb end;

} Streamer;


Streamer* Streamer_create(Ctx *ctx, strategy_cb strategy_cb, data_cb data_cb, end_cb end_cb, size_t BUF_LEN);
int Streamer_on_data_available(int fd, Streamer *parser);
void Streamer_destroy(Streamer *parser);
