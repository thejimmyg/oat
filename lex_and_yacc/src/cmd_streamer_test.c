#include "streamer.h"
#include "queue.h"
#include "james.h"

#include <getopt.h>
#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <signal.h>

#define INIT_CTX_TYPE 1000
#define BUF_LEN 7 /* Read buffer in bytes */


typedef struct {
    Ctx super;
    Queue* queue_out;
} InitCtx;


void help(char c) {
    if (c != 'h') {
        log_error("error");
    }
    debug("This is the help text");
}


void on_end(Ctx *ctx, Cmd *cmd) {
    assert(ctx->type == INIT_CTX_TYPE);
    InitCtx *initctx = (InitCtx*)ctx;
    debug("On end");
    if (strncmp("user", cmd->name, cmd->name_len + 1) == 0) {
        char *buf = "oat resp --ok\n";
        Queue_add(initctx->queue_out, buf, strlen(buf), 0);
    }
}

void on_data(Ctx *ctx, Cmd *cmd, const char* byte) {
    assert(ctx->type == INIT_CTX_TYPE);
    InitCtx *initctx = (InitCtx*)ctx;
    debug("Got one byte: %.*s", 1, byte);
}

void on_strategy(Ctx *ctx, Cmd *cmd, int strategy, size_t length) {
    int x;
    log_info("Args found: %zu", cmd->args->used);
    for (x=0; x<cmd->args->used; x++) {
        debug("Arg %d, (%zu bytes): -->%s<--", x, cmd->args->item_sizes[x], cmd->args->array[x]);
    }
    assert(ctx->type == INIT_CTX_TYPE);
    InitCtx *initctx = (InitCtx*)ctx;
    // We'll include the null-terminator in the comparison to stop 'init' matching 'initialize' for example:
    if (strncmp("init", cmd->name, cmd->name_len + 1) == 0) {
        debug("Handling init");
        debug("Done handling init");
    } else if (strncmp("user", cmd->name, cmd->name_len + 1) == 0) {
        debug("Handling user %s with %zu args", cmd->args->array[0], cmd->args->used);
    }
}


int main (int argc, char **argv) {
    exit(0);

    // test_queue();
    int c;
    int digit_optind = 0;
    int aopt = 0, bopt = 0;
    char *copt = 0, *dopt = 0;
    static struct option long_options[] = {
        {"help", no_argument, NULL, 'h'},
        {NULL, 0, NULL, 0}
    };
    int option_index = 0;
    while ((c = getopt_long(argc, argv, "h",
       long_options, &option_index)) != -1) {
        int this_option_optind = optind ? optind : 1;
        switch (c) {
        // case 0:
        //     debug ("option %s", long_options[option_index].name);
        //     if (optarg)
        //             debug (" with arg %s", optarg);
        //     debug ("\n");
        //     break;
        case 'h':
            help(c);
            return 0;
        case '?':
            break;
        default:
            help(c);
        }
    }
    if (optind < argc) {
        log_error("Unexpected arguments");
        return 4;
        // debug ("non-option ARGV-elements: ");
        // while (optind < argc)
        //     debug ("%s ", argv[optind++]);
        // debug ("\n");
    } else {
        //debug("optind: %d", optind);
        /* Global variable */
        return run()
    }
}

