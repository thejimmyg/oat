Oat
===

Abandoned
---------

Parsing algorithm
~~~~~~~~~~~~~~~~~

If you run `make benchmark` you'll see that the pipeline version fails.

Description of the problem:

    Intermittently when parsing around 1,000,000 messages, the parser mis-parses the input.

From debugging what appears to be happening is this:

* The parser works fine until the buffer is full
* When it wraps around, the code that shuffles the remaining text to the front of the buffer works correctly
* The ragel parser gets confused and thinks the whole input is still part of the last command
* This means the marked start point is the first byte in the buffer, so the whole buffer becomes marked as part of the same command and we get an error saying that there is no space in the buffer to parse more content

The error is correct, but it should not be caused, beacuse Ragel should notice.

I can only produce this with 1024 byte buffers not the 7 byte ones used in the test which work correctly.

Looking at other tools, it appears that they generally take this approach:

* Put some data in the buffer
* Try to parse it
  If you get a command:
     * Shuffle down the rest of the bytes in the buffer
     * Contunie
  Else
     * Read another chunk of data into the buffer and try again
* If the buffer becomes full or after a few attempts the parsing is not successful, give up

This algorithm is nice because it doesn't require any pointer tricks in the parser code. You just start again. Since the reading and writing is probably the slow bit, one or two wasted parse attempts at the end of the buffer isn't the end of the world. Also it gives the ability to break out of the parser to handle the next bit of input in a different way (perhaps read a set number of bytes, look for an EOF marker, or parse the body of the response).

The problem is that if you are going to take this approach, you no longer need a streaming parser. You can just use plain Lex+Yacc on the whole input buffer and just try again if they fail (which is what you are effectively forced to do in Ragel in this circumstance anyway). If you are going to do that, you might as well use jison and not C. Then your parser works both ends of a connection.


Use of pipes
~~~~~~~~~~~~

One of the problems with pipes is that you have to exit when there is an error. That's rather annoying. Using a tcp connection is better as you can just close it.

Conclusions
~~~~~~~~~~~

Doing this again I would use lex and yacc, take a trial and error approach and use tcp connections, not named pipes.

Here's a server example (use two telnet clients to chat):

* http://beej.us/guide/bgnet/examples/selectserver.c

The books is brilliant generally:

* http://beej.us/guide/bgnet/

What about the libgit+filesystem thinking?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Don't know! There is also PouchDB with a LMDB back-end.


Build
-----

~~~
make && make test
~~~

Protocol
--------

Our protocol looks like this in the complex case:

~~~
oat user -a -b --one hello "wo""rld" << EOF
one
EOF
oat user -a -b --one hello "wo""rld" << 3
two
oat init
~~~

The idea is to define a start action:

~~~
action mark { ... }
~~~

and then an end action for each interesting thing:

~~~
action short_option {
    string("Short option", fpc, parser);
    if (parser->short_option != NULL)
        parser->short_option(parser->cmd, parser->start_pos, GET_LEN(fpc));
}
~~~

then define a machine like this:

~~~
short_option = '-' [a-zA-Z]+ >mark %short_option;
~~~

The macros ensure that the callback is called with the right data.

Network
-------

To test across a network you can do this. For the client:

~~~
time ./release/oat_echo_client - <test.pipe | nc 192.168.0.6 5000 > test.pipe
~~~

Then on the server:

~~~
nc -l -p 5000 0<test.pipe | ./release/oat_echo_server > test.pipe
~~~

For a pipelineing test, you can try this on the client instead:

~~~
python gen.py > input
time cat input | nc 192.168.0.6 5000 > /dev/null
~~~
