Cases to deal with:


success:
  * read()

error:
  errorcount
  last_err_pos

  errorcount++
  if errorcount == max:
      return -2
  else if errorcount > 1 and last_err_place == this_err_place:
      return -1 # What about no read? EOF still means error.
  else:
      if buffer_full (or less than REALISTIC_READ_SIZE):
          shuffle
          read and parse()
      else 
          read and parse()



Empty buffer:
  * Short read
    read again
  * Exact read
    parse read again 
  * Long read
    parse read again 


Partial buffer:

  * Short read
    read again
  * Exact read
    parse read again 
  * Long read
    parse read again 

Full buffer:

  * Exact read
    parse, read again
