%{
#include <stdio.h>
#include <string.h>
#include "rfc822_parser.h"


//int yydebug = 1;
///* See http://www.ibm.com/developerworks/library/l-flexbison/ */
//#define YYERROR_VERBOSE 1

void rfc822_parser_yyerror(const char *str)
{
    log_info("Rfc822Parser error: %s\n", str);
    if (rfc822p->err_string == NULL) {
        rfc822p->err_string = str;
    }
//    rfc822p->lex_error = 1;
//    printf("\nerror: %s\n", str);
}

int rfc822_parser_yywrap()
{
    return 1;
}
%}


%token TOKSPACE TOKSEPARATOR TOKNEWLINE ERROR

%union
{
	int number;
	char *string;
}

%token <string> STRING

%start doc

%%

doc
    : error {
        printf("Called error");
        YYABORT;
    }
    | lines TOKNEWLINE { YYACCEPT; }
    ;

lines
    : line
    | lines TOKNEWLINE line
    ;

line 
    : STRING TOKSPACE TOKSEPARATOR TOKSPACE STRING TOKSPACE {
        if (strncmp("hello", $1, 5) == 0) {
            rfc822p->says_hello = 1;
        }
        if (strncmp("hello", $5, 5) == 0) {
            rfc822p->says_hello = 1;
        }
    }
    | STRING TOKSEPARATOR TOKSPACE STRING TOKSPACE {
        if (strncmp("hello", $1, 5) == 0) {
            rfc822p->says_hello = 1;
        }
        if (strncmp("hello", $4, 5) == 0) {
            rfc822p->says_hello = 1;
        }
    }
    | STRING TOKSEPARATOR TOKSPACE STRING {
        if (strncmp("hello", $1, 5) == 0) {
            rfc822p->says_hello = 1;
        }
        if (strncmp("hello", $4, 5) == 0) {
            rfc822p->says_hello = 1;
        }
    }
    | STRING TOKSEPARATOR STRING TOKSPACE {
        if (strncmp("hello", $1, 5) == 0) {
            rfc822p->says_hello = 1;
        }
        if (strncmp("hello", $3, 5) == 0) {
            rfc822p->says_hello = 1;
        }
    }
    | STRING TOKSEPARATOR STRING{
        if (strncmp("hello", $1, 5) == 0) {
            rfc822p->says_hello = 1;
        }
        if (strncmp("hello", $3, 5) == 0) {
            rfc822p->says_hello = 1;
        }
    }
    ;

