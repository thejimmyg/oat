#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "rfc822_parser.h"
#include "james.h"

typedef struct Fixture Fixture;
struct Fixture {
    char* buf;
    size_t len;
};


void dump(char* buf, size_t len) {
    size_t i;
    for (i=0; i<len; i++) {
        if (buf[i] == '\n') {
            printf("\\n");
        } else if (buf[i] == '\0') {
            printf("N");
        } else {
            printf("%c", buf[i]);
        } 
    }
}


Fixture* setup() {
    Fixture* fixture = (Fixture *) malloc (sizeof(Fixture));
    char* buf = (char *) malloc (sizeof(char) * 14);
    memset(buf,'$', 14);
    fixture->buf = buf;
    fixture->len = 14;
    return fixture;
}


void teardown(Fixture* fixture) {
    free(fixture->buf);
    free(fixture);
}


int test_error(Fixture* f) {
    char* in = f->buf;
                  //0123456789
    memcpy(f->buf, "no: +waay\n", 10);
    Rfc822Parser_expect(in, f->buf);
    Rfc822Parser* rfc822_parser = Rfc822Parser_create(f->buf, 14, 10);
    Rfc822Parser_expect(rfc822_parser->res, 1);
    Rfc822Parser_expect(rfc822_parser->cur_offset, 4);
    printf("Res: >"); dump(f->buf, 14); printf("<\n");
    Rfc822Parser_expect(memcmp(f->buf, "no: +waay\n$$\0\0", 14), 0);
    Rfc822Parser_expect(rfc822_parser->says_hello, 0);
    return 0;
}

int test_success(Fixture* f) {
    char* in = f->buf;
                  //0123456789
    memcpy(f->buf, "hello: al\n", 10);
    Rfc822Parser_expect(in, f->buf);
    printf("Res: >"); dump(f->buf, 14); printf("<\n");
    Rfc822Parser* rfc822_parser = Rfc822Parser_create(f->buf, 14, 10);
    printf("Res: >"); dump(f->buf, 14); printf("<\n");
    Rfc822Parser_expect(rfc822_parser->res, 0);
    Rfc822Parser_expect(rfc822_parser->cur_offset, 9);
    Rfc822Parser_expect(memcmp(f->buf, "hello: al\n$$\0\0", 14), 0);
    Rfc822Parser_expect(rfc822_parser->says_hello, 1);
    return 0;
}


int main(){

    Fixture* f;

    printf("\nError Test\n==========\n\n");
    f = setup();
    test_error(f);
    teardown(f);
    printf("PASSED\n\n");

    printf("Success Test\n============\n\n");
    f = setup();
    test_success(f);
    teardown(f);
    printf("PASSED\n\n");

    return 0;
}

